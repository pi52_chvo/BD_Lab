use torgfirm

select sum(tovar.Zap_skl) as zag_kilkist from tovar

select count(Sotrudnik.id_sotrudnik) as sot_kilkist from Sotrudnik

select count(postachalnik.id_postach) as post_kilkist from postachalnik

select sum(zakaz_tovar.kilk), tovar.Nazva_t from tovar
	inner join zakaz_tovar on tovar.id_tovar = zakaz_tovar.id_tovar
	inner join Zakaz on zakaz_tovar.id_zakaz = Zakaz.K_zakaz
		where ((year(Zakaz.Date_naznach) = year(getdate())) and (month(Zakaz.Date_naznach) = (month(getdate()) - 1)))
	 group by tovar.Nazva_t 

select sum(Qresult2.Itog) as itog from Qresult2
	where ((year(Qresult2.Date_naznach) = year(getdate())) and (month(Qresult2.Date_naznach) = (month(getdate()) - 1)))

select sum(Qresult2.Itog) as sum_itog, postachalnik.Nazva from postachalnik
	inner join tovar on tovar.id_postach = postachalnik.id_postach
	inner join zakaz_tovar on zakaz_tovar.id_tovar = tovar.id_tovar
	inner join Qresult2 on Qresult2.id_zakaz = zakaz_tovar.id_zakaz
	 group by postachalnik.Nazva

select count(Zakaz.K_zakaz) as kilkist, postachalnik.Nazva from Zakaz
	inner join zakaz_tovar on  Zakaz.K_zakaz = zakaz_tovar.id_zakaz
	inner join tovar on tovar.id_tovar = zakaz_tovar.id_tovar
	inner join postachalnik on postachalnik.id_postach = tovar.id_postach
		where tovar.Nazva_t like '����'
			group by postachalnik.Nazva

select tovar.Nazva_t, avg(tovar.price*zakaz_tovar.kilk*(1 - zakaz_tovar.znigka)) as avg_price from tovar
	inner join zakaz_tovar on zakaz_tovar.id_tovar = tovar.id_tovar
	 group by tovar.Nazva_t

select klient.Nazva, sum(tovar.price*zakaz_tovar.kilk*(1 - zakaz_tovar.znigka)) as total_price from klient
	inner join Zakaz on klient.id_klient = Zakaz.K_klien
	inner join zakaz_tovar on  zakaz_tovar.id_zakaz = Zakaz.K_zakaz
	inner join tovar on tovar.id_tovar = zakaz_tovar.id_tovar
		where klient.City like '�. �������'
		 group by klient.Nazva

select postachalnik.Nazva, avg(tovar.price*zakaz_tovar.kilk*(1 - zakaz_tovar.znigka)) as avg_price from postachalnik
	inner join tovar on  tovar.id_postach = postachalnik.id_postach
	inner join zakaz_tovar on  tovar.id_tovar = zakaz_tovar.id_tovar
		group by postachalnik.Nazva
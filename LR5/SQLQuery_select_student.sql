--1--
select dbo_student.Name, dbo_student.Fname, Sum(Reiting.Reiting) as sum from dbo_student
	inner join Reiting on Reiting.Kod_student = dbo_student.Kod_stud
	inner join Rozklad_pids on Rozklad_pids.K_zapis = Reiting.K_zapis
	inner join Predmet_plan on Predmet_plan.K_predm_pl = Rozklad_pids.K_predm_pl
	inner join predmet on predmet.K_predmet = Predmet_plan.K_predmet
	 group by dbo_student.Name, dbo_student.Fname

--2--
select dbo_groups.Kod_group, count(dbo_student.Kod_stud) as student_count from dbo_groups
	left join dbo_student on dbo_student.Kod_group = dbo_groups.Kod_group
		group by dbo_groups.Kod_group

--3--
select dbo_groups.Kod_group, count(predmet.K_predmet) as predmet_count from dbo_groups
	inner join Rozklad_pids on Rozklad_pids.Kod_group = dbo_groups.Kod_group
	inner join Predmet_plan on Predmet_plan.K_predm_pl = Rozklad_pids.K_predm_pl
	inner join predmet on predmet.K_predmet = Predmet_plan.K_predmet
		group by dbo_groups.Kod_group

--6--
select predmet.Nazva, avg(Reiting.Reiting) as avg_bal from predmet
	inner join Predmet_plan on Predmet_plan.K_predmet = predmet.K_predmet
	inner join Rozklad_pids on Rozklad_pids.K_predm_pl = Predmet_plan.K_predm_pl
	inner join Reiting on Reiting.K_zapis = Rozklad_pids.K_zapis
		group by predmet.Nazva

--5--
select dbo_groups.Kod_group, avg(Reiting.Reiting) as avg_reiting from dbo_groups
	inner join dbo_student on dbo_groups.Kod_group = dbo_student.Kod_group
	inner join Reiting on Reiting.Kod_student = dbo_student.Kod_stud
	 group by dbo_groups.Kod_group


-- 4--
select g.Kod_group, sum(Predmet_plan.Chas_all) as countOfLessons from dbo_groups as g
	left join Navch_plan on Navch_plan.K_navch_plan = g.K_navch_plan
	left join Predmet_plan on  Predmet_plan.K_navch_plan = Navch_plan.K_navch_plan
		group by g.Kod_group;


--7--
select predmet.Nazva, Rozklad_pids.Date, dbo_student.Name, dbo_student.Fname, Reiting.Reiting as sum from dbo_student
	inner join Reiting on Reiting.Kod_student = dbo_student.Kod_stud
	inner join Rozklad_pids on Rozklad_pids.K_zapis = Reiting.K_zapis
	inner join Predmet_plan on Predmet_plan.K_predm_pl = Rozklad_pids.K_predm_pl
	inner join predmet on predmet.K_predmet = Predmet_plan.K_predmet
	 order by Rozklad_pids.Date desc
	  
--8--
select min(Reiting.Reiting) as minimum, predmet.Nazva from Reiting
	inner join dbo_student on dbo_student.Kod_stud = Reiting.Kod_student
	inner join Rozklad_pids on Rozklad_pids.K_zapis = Reiting.K_zapis
	inner join Predmet_plan on Predmet_plan.K_predm_pl = Rozklad_pids.K_predm_pl
	inner join predmet on predmet.K_predmet = Predmet_plan.K_predmet
	 group by  predmet.Nazva

--9--
select max(Reiting.Reiting) as minimum, predmet.Nazva from Reiting
	inner join dbo_student on dbo_student.Kod_stud = Reiting.Kod_student
	inner join Rozklad_pids on Rozklad_pids.K_zapis = Reiting.K_zapis
	inner join Predmet_plan on Predmet_plan.K_predm_pl = Rozklad_pids.K_predm_pl
	inner join predmet on predmet.K_predmet = Predmet_plan.K_predmet
	 group by  predmet.Nazva


-- 10--

select Nazva, f.V_form, sum(Predmet_plan.Chas_all) as sum_hours from predmet
	inner join Predmet_plan on Predmet_plan.K_predmet = predmet.K_predmet
	inner join Navch_plan on Navch_plan.K_navch_plan = Predmet_plan.K_navch_plan
	inner join Form_navch as f on f.K_form = Navch_plan.k_form
		group by Nazva, f.V_form;

--11--
select Spetsialnost.Nazva, count(dbo_groups.Kod_group) from Spetsialnost
 left join Navch_plan on Spetsialnost.K_spets = Navch_plan.K_spets
 left join Predmet_plan on Predmet_plan.K_navch_plan = Navch_plan.K_navch_plan
 left join Rozklad_pids on Rozklad_pids.K_predm_pl = Predmet_plan.K_predm_pl
 left join dbo_groups on dbo_groups.Kod_group = Rozklad_pids.Kod_group
	group by Spetsialnost.Nazva


CREATE VIEW Qresult AS
SELECT zakaz_tovar.id_zakaz, klient.Nazva, zakaz_tovar.id_tovar, zakaz_tovar.kilk, zakaz_tovar.znigka, tovar.price*zakaz_tovar.kilk*(1 - zakaz_tovar.znigka) AS Zag_vartist
	FROM (klient INNER JOIN Zakaz ON klient.id_klient = Zakaz.K_klien)
		INNER JOIN (tovar INNER JOIN zakaz_tovar ON tovar.id_tovar = zakaz_tovar.id_tovar) ON Zakaz.K_zakaz = zakaz_tovar.id_zakaz;

select Sotrudnik.Fname, Sotrudnik.Name, Sotrudnik.Posada, avg(Qresult2.Itog) as avg_itog
	from (Sotrudnik inner join Zakaz on Sotrudnik.id_sotrudnik = Zakaz.K_sotrud)
		inner join Qresult2 on Zakaz.K_zakaz = Qresult2.id_zakaz
			group by Sotrudnik.Fname, Sotrudnik.Name, Sotrudnik.Posada

select top(3) Sotrudnik.Fname, Sotrudnik.Name, Sotrudnik.Posada, Qresult.Zag_vartist
	from (Sotrudnik inner join Zakaz on Sotrudnik.id_sotrudnik = Zakaz.K_sotrud)
		inner join Qresult on Zakaz.K_zakaz = Qresult.id_zakaz
			group by Sotrudnik.Fname, Sotrudnik.Name, Sotrudnik.Posada, Qresult.Zag_vartist
			 order by Qresult.Zag_vartist desc

select klient.Nazva, sum(Qresult.Zag_vartist) as sum_zag_vartist
	from (klient inner join Zakaz on Zakaz.K_klien = klient.id_klient) 
		inner join Qresult on Zakaz.K_zakaz = Qresult.id_zakaz
		 group by klient.Nazva

select tovar.Nazva_t, max((tovar.price*zakaz_tovar.kilk*(1 - zakaz_tovar.znigka))) as zag_vartist
	from tovar inner join zakaz_tovar on tovar.id_tovar = zakaz_tovar.id_tovar 
		group by tovar.Nazva_t


select Sotrudnik.Fname, Sotrudnik.Name, Sotrudnik.Posada, count(zakaz_tovar.id_zakaz) as count_zakaz
	from Zakaz inner join Sotrudnik on Sotrudnik.id_sotrudnik = Zakaz.K_sotrud
		inner join zakaz_tovar on Zakaz.K_zakaz = zakaz_tovar.id_zakaz
		group by Sotrudnik.Fname, Sotrudnik.Name, Sotrudnik.Posada
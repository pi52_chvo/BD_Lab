create view Qresult2 as
select Qresult.id_zakaz, klient.Nazva, Zakaz.Date_naznach, sum(Qresult.Zag_vartist) as Itog
	from(klient inner join Zakaz on klient.id_klient = Zakaz.K_klien)
		inner join Qresult on Zakaz.K_zakaz = Qresult.id_zakaz
			group by Qresult.id_zakaz, klient.Nazva, Zakaz.Date_naznach;

 create database torg_firm;
 go
USE torg_firm;
GO


CREATE TABLE [dbo].[klient](
	[K_klient] [int] Not NULL primary key ,
	[Nazva] [varchar](50) NULL,
	[Adress] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[Region] [varchar](50) NULL,
	[Country] [varchar](50) NULL,
	[Tel] [varchar](50) NULL,
	[Fax] [varchar](50) NULL,
	[Web_site] [char](20) NULL,
	[Kontakt_osoba] [varchar](50) NULL,
	[Posada] [varchar](50) NULL
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[postachalnik](
	[K_postach] [int] not NULL primary key,
	[Nazva] [varchar](50) NULL,
	[Adress] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[Region] [varchar](50) NULL,
	[Country] [varchar](50) NULL,
	[Tel] [varchar](50) NULL,
	[Fax] [varchar](50) NULL,
	[Web_site] [varchar](20) NULL,
	[Kontakt_osoba] [varchar](50) NULL,
	[Posada] [varchar](50) NULL
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[tovar](
	[K_tovar] [int] IDENTITY(1,1) NOT NULL primary key,
	[Nazva] [varchar](50) NULL,
	[Opis] [varchar](50) NULL,
	[Price] [decimal](19, 4) NULL,
	[NaSklade] [int] NULL,
	[Postavka] [int] NULL,
	[Min_zapas] [int] NULL,
	[Post_prek] [bit] NULL,
	[K_postav] [int] not NULL references [dbo].[postachalnik]([K_postach]))

	Go

CREATE TABLE [dbo].[sotrudnik](
	[K_sotrud] [int] not NULL primary key,
	[Fname] [varchar](50) NULL,
	[Name] [varchar](50) NULL,
	[Posada] [varchar](50) NULL,
	[Date_birth] [datetime] NULL,
	[Datr_enter] [datetime] NULL,
	[Adress] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[Region] [varchar](50) NULL,
	[Country] [varchar](50) NULL,
	[Home_tel] [varchar](50) NULL,
	[Fax] [varchar](50) NULL,
	[Foto] [varbinary](20) NULL,
	[Primitka] [varchar](20) NULL
) 

GO

CREATE TABLE [dbo].[zakaz](
	[K_zakaz] [int] not null primary key,
	[K_klient] [int] not null references [dbo].[klient]([K_klient]),
	[k_sotrud] [int] not NULL references [dbo].[sotrudnik]([K_sotrud]),
	[date_rozm] [datetime] NULL,
	[date_naznach] [datetime] NULL
) 

GO
CREATE TABLE [dbo].[zakaz_tovar](
	[K_zakaz] [int] not NULL references [dbo].[zakaz]([K_zakaz]),
	[k_tovar] [int] not NULL references [dbo].[tovar]([K_tovar]),
	[Kilkist] [int] NULL,
	[Znigka] [int] NULL,
	primary key ([K_zakaz] , [k_tovar])
) 

GO

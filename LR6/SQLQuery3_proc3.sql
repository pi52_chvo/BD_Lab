create procedure proc3
as
update Reiting set Reiting.Reiting = (Reiting.Reiting + 1.15)
	where Reiting.K_zapis in (select Rozklad_pids.K_zapis from Rozklad_pids where month(Rozklad_pids.Date) = 12)

go 
execute proc3
select * from Reiting inner join Rozklad_pids on Rozklad_pids.K_zapis = Reiting.K_zapis


create procedure proc7
@predmet as varchar, @vid as int
as
update Rozklad_pids set Rozklad_pids.N_vedomost = @vid
	where Rozklad_pids.K_predm_pl in (
	select Predmet_plan.K_predm_pl from Predmet_plan
		inner join predmet on predmet.K_predmet = Predmet_plan.K_predmet
			where predmet.Nazva = @predmet)

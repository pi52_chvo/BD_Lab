/****** Скрипт для команды SelectTopNRows из среды SSMS  ******/
INSERT INTO Degree(Name) VALUES('Доцент')
INSERT INTO Degree(Name) VALUES('Професор')
SELECT * FROM Degree

INSERT INTO AcademicStatus(Name) VALUES('Кандидат наук')
INSERT INTO AcademicStatus(Name) VALUES('Доктор наук')
SELECT * FROM AcademicStatus

INSERT INTO Course(Name, Faculty) VALUES ('Курс1', 'ІКТ'), ('Курс1', 'ЕМ'), ('Курс1', 'ІКТ'), ('Курс1', 'ІМ')
UPDATE Course SET Name = 'Курс2' WHERE CourseId=3
SELECT * FROM Course

INSERT INTO Groupp(Name, CourseId) VALUES ('ПІ-52', 5), ('ПІ-47', 3)
SELECT * FROM Groupp

INSERT INTO Loadd(Name) VALUES ('Лекція'), ('Практика'), ('Захист курсової'), ('Захист диплому')
SELECT * FROM Loadd

INSERT INTO Teacher(Firstname, Lastname, Midlename, Phonenumber, Familystatus, StatusId, DegreeId)
	VALUES ('Вова', 'Петренко', 'Олександрович', '+380541753429', 'Одружений', 2, 1),
	('Дмитро', 'Коваленко', 'Петрович', '+380663542365', 'Неодружений', 1, 1),
	('Тарас', 'Лисюк', 'Дмитрович', '+380925433762', 'Одружений', 2, 2)
SELECT * FROM Teacher

INSERT INTO Subjects(Name) VALUES ('Програмування'),('Матаналіз'), ('Вебдизайн')
SELECT * FROM Subjects

INSERT INTO Teacher_Load (TeacherId, SubjectId, LoadId, GroupId, LoadHour, DataHour) 
	VALUES (4, 2, 1, 3, 10, 334),
	(5, 2, 2, 3, 34, 44) 
UPDATE Teacher_Load SET DataHour = 2016 WHERE TeacherId = 5 AND GroupId = 3
SELECT * FROM Teacher_Load

SELECT TOP(1) * FROM Teacher INNER JOIN Teacher_Load ON Teacher.TeacherId = Teacher_Load.TeacherId 
	WHERE Teacher_Load.LoadHour = 10 
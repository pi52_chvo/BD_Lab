create table postachalnik
 (id_postach int IDENTITY(1,1) not null primary key,
 Nazva varchar(20) constraint nazva_format check (Nazva like '[�-�]%'),
 Index_p char(5) constraint ind_format check (index_p like '[0-9][0-9][0-9][0-9][0-9]')
default 100000,
 Addr varchar(50),
 City varchar(20) constraint city_format check (City like '�. [�-�]%'),
 Region varchar(50),
 tel char(12) check (tel like '([0-9][0-9][0-9])[0-9][0-9][0-9][0-9][0-9][0-9][0-9]')) create table tovar
 (id_tovar int IDENTITY(1,1) not null primary key,
 Nazva_t varchar(20) constraint nazva_tf check (Nazva_t like '[�-�]%'),
 Min_zap int check (Min_zap >0) default 10,
 Zap_skl int,
 price money,
 post_prek smallint check (post_prek in (0,1)),
 id_postach int not null foreign key (id_postach) references
dbo.postachalnik(id_postach) on delete cascade on update no action
 )

 create table klient
 (id_klient int IDENTITY(1,1) not null primary key,
 Nazva varchar(20) constraint klient_nazva_format check (Nazva like '[�-�]%'),
 Index_p char(5) constraint klient_ind_format check (index_p like '[0-9][0-9][0-9][0-9][0-9]')
default 100000,
 Addr varchar(50),
 City varchar(20) constraint klient_city_format check (City like '�. [�-�]%'),
 Region varchar(50),
 tel char(12) check (tel like '([0-9][0-9][0-9])[0-9][0-9][0-9][0-9][0-9][0-9][0-9]')) Create table Sotrudnik(
id_sotrudnik int identity(1,1) not null primary key,
Fname varchar(20) constraint sot_Fname_format check (Fname like '[�-�]%') ,
Name varchar(20) constraint sot_name_format check (name like '[�-�]%') ,
Posada varchar(40) constraint sot_posada_format check (posada like '[�-�]%') ,
Date_enter date,
Date_birthday date,
Index_p char(5) constraint sot_ind_format check (index_p like '[0-9][0-9][0-9][0-9][0-9]')
default 100000,
Adress varchar(50) ,
City varchar(20) constraint sot_city_format check (city like '[�-�]%') ,
Region varchar(20) constraint sot_region_format check (region like '[�-�]%') ,
Country varchar(20) constraint sot_country_format check (country like '[�-�]%') ,
Tel char (10) check (tel like '([0-9][0-9][0-9])[0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
Foto varchar(50),
Note text)create table zakaz_tovar(
id_zakaz int not null references Zakaz(K_zakaz ) on update cascade on delete cascade,
id_tovar int not null references tovar(id_tovar) on update no action on delete cascade,
kilk int check (kilk>0),
znigka decimal (3,2)
primary key (id_zakaz, id_tovar))Create table Zakaz(
K_zakaz int identity(1, 1) primary key,
K_klien int references klient(id_klient),
K_sotrud int references sotrudnik(id_sotrudnik),
Date_rozm date default getdate(),
Date_naznach date default getdate()+10)
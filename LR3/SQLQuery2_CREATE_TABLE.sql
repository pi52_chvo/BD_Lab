CREATE TABLE Teacher(
	TeacherId INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	Firstname NVARCHAR(50) CONSTRAINT firstname_check CHECK (Firstname Like '[�-��-�]%'),
	Lastname NVARCHAR(50) CONSTRAINT lastname_check CHECK (Lastname Like '[�-��-�]%'),
	Midlename NVARCHAR(50) CONSTRAINT midlename_check CHECK (Midlename Like '[�-��-�]%'),
	Phonenumber NVARCHAR(13) CONSTRAINT phone_check CHECK (Phonenumber Like '+380[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
	Familystatus NVARCHAR(50) CONSTRAINT familystatus_check CHECK (Familystatus IN ('���������', '�����������', '������', '��������')),
	Birthday DATETIME DEFAULT (DATEADD(year, -30, GETDATE())),
	StatusId INT,
	DegreeId INT,
	CONSTRAINT references_to_status FOREIGN KEY (StatusId) REFERENCES AcademicStatus(StatusId) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT references_to_degree FOREIGN KEY (DegreeId) REFERENCES Degree(DegreeId) ON UPDATE CASCADE ON DELETE CASCADE
)

CREATE TABLE AcademicStatus (
	StatusId INT NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Name NVARCHAR(50) CONSTRAINT name_check CHECK(Name Like '[�-��-�]%')
)

CREATE TABLE Degree (
	DegreeId INT NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Name NVARCHAR(50) CONSTRAINT degree_name_check CHECK(Name Like '[�-��-�]%')
)

CREATE TABLE Course (
	CourseId INT NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Name NVARCHAR(50) CONSTRAINT course_name_check CHECK(Name Like '[�-��-�]%'),
	Faculty NVARCHAR(50) CONSTRAINT course_faculty_check CHECK(Faculty Like '[�-��-�]%'),
)

CREATE TABLE Groupp (
	GroupId INT NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Name NVARCHAR(50) CONSTRAINT group_name_check CHECK(Name Like '[�-��-�]%'),
	CourseId INT NOT NULL,
	CONSTRAINT references_to_course FOREIGN KEY(CourseId) REFERENCES Course(CourseId) ON UPDATE CASCADE ON DELETE CASCADE
)

CREATE TABLE Loadd (
	LoadId INT NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Name NVARCHAR(50) CONSTRAINT load_name_check CHECK(Name Like '[�-��-�]%')
)

CREATE TABLE Teacher_Load(
	TeacherId INT NOT NULL,
	SubjectId INT NOT NULL,
	LoadId INT NOT NULL,
	GroupId INT NOT NULL,
	LoadHour FLOAT NOT NULL,
	DataHour INT NOT NULL CONSTRAINT data_check DEFAULT( 2 ), 
	CONSTRAINT references_to_teacher FOREIGN KEY(TeacherId) REFERENCES Teacher(TeacherId) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT references_to_load  FOREIGN KEY(LoadId) REFERENCES Loadd(LoadId) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT refernces_to_group  FOREIGN KEY(GroupId) REFERENCES Groupp(GroupId) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT refernces_to_subject  FOREIGN KEY(SubjectId) REFERENCES Subjects(SubjectId) ON UPDATE CASCADE ON DELETE CASCADE,
)

CREATE TABLE Subjects(
	SubjectId INT NOT NULL IDENTITY(1, 1) PRIMARY KEY,
	Name NVARCHAR(50) CONSTRAINT subject_name CHECK (Name LIKE '[�-��-�]%')
)
﻿using curs.Models.Role;
using curs.Models.Worker;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Hangfire;
using Hangfire.SqlServer;
using curs.Services;
using curs.Repositoryes;
using System.Web.Mvc;

[assembly: OwinStartup(typeof(curs.App_Start.Startup))]

namespace curs.App_Start
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.CreatePerOwinContext<HotelContext>(() => new HotelContext());
            app.CreatePerOwinContext<WorkerManager>(WorkerManager.Create);
            app.CreatePerOwinContext<HotelRoleManager>(HotelRoleManager.Create);
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Workers/Login"),
            });
            GlobalConfiguration.Configuration.UseSqlServerStorage("HotelConnection");
            RecurringJob.AddOrUpdate(() => DependencyResolver.Current.GetService<RoomService>().SetRoomStatus(), Cron.Hourly);
            app.UseHangfireDashboard();
            app.UseHangfireServer();
        }
    }
}
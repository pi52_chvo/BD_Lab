﻿using curs.Models.Request;
using curs.Models.Room;
using curs.Repositoryes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace curs.Services
{
    public class RoomService
    {

        public RoomService()
        {
        }

        public void SetRoomStatus()
        {
            EFUnitOfWork db = new EFUnitOfWork();
            List<Request> requestsDeparture = db.Requests.Get().Where(r => r.DepartureDate.Month == DateTime.Now.Month && r.DepartureDate.Year == DateTime.Now.Year && r.DepartureDate.Day == DateTime.Now.Day && r.DepartureDate.Hour == DateTime.Now.Hour).ToList();
            foreach (var item in requestsDeparture)
            {
                List<Room> rooms = item.Rooms;
                foreach (var room in rooms)
                {
                    Room r = db.Rooms.GetById(room.Id);
                    r.IsFree = true;
                    db.Rooms.Update(r);
                }
            }
            List<Request> requestsArrival = db.Requests.Get().Where(r => r.ArrivalDate.Month == DateTime.Now.Month && r.ArrivalDate.Year == DateTime.Now.Year && r.ArrivalDate.Day == DateTime.Now.Day && r.ArrivalDate.Hour == DateTime.Now.Hour).ToList();
            foreach (var item in requestsArrival)
            {
                List<Room> rooms = item.Rooms;
                foreach (var room in rooms)
                {
                    Room r = db.Rooms.GetById(room.Id);
                    r.IsFree = false;
                    db.Rooms.Update(r);
                }
            }
            db.Save();
            db.Dispose();
        }
    }
}
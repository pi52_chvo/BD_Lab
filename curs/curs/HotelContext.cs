﻿using curs.Models;
using curs.Models.Client;
using curs.Models.Request;
using curs.Models.Role;
using curs.Models.Room;
using curs.Models.RoomType;
using curs.Models.Service;
using curs.Models.Worker;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace curs
{
    public class HotelContext : IdentityDbContext<Worker>
    {
        public DbSet<Room> Rooms { get; set; }
        public DbSet<RoomType> RoomTypes { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Request> Requests { get; set; }
        public DbSet<OrderedRoomType> OrderedRoomTypes { get; set; }
        public DbSet<OrderedService> OrderedServices { get; set; }
        public override IDbSet<Worker> Users { get; set; }
        public DbSet<HotelRole> IdentityRoles { get; set; }

        public HotelContext()
            : base("HotelConnection")
        {
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace curs.Models.RoomType
{
    public class RoomTypeCreateModel
    {
        [Required]
        [StringLength(15, MinimumLength = 2)]
        public string Title { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        public double Price { get; set; }
    }
}
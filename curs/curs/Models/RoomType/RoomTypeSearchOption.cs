﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace curs.Models.RoomType
{
    public class RoomTypeSearchOption
    {
        public string Title { get; set; }

        public string Price { get; set; }
    }
}
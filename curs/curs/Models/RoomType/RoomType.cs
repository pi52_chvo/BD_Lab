﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using curs.Models.Room;

namespace curs.Models.RoomType
{
    public class RoomType
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(15),MinLength(2)]
        public string Title { get; set; }

        [Required]
        public double Price { get; set; }

        public virtual List<Room.Room> Rooms { get; set; }

        public virtual List<OrderedRoomType> OrderedRoomTypes { get; set; }
    }
}
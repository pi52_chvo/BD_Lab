﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace curs.Models.RoomType
{
    public class OrderedRoomType : IEquatable<OrderedRoomType>
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(15), MinLength(2)]
        public string Title { get; set; }

        [Required]
        public double Price { get; set; }

        public RoomType RoomType { get; set; }

        public virtual List<Request.Request> Requests { get; set; }

        public bool Equals(OrderedRoomType other)
        {
            return this.Id == other.Id;
        }
    }
}
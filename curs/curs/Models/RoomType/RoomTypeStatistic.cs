﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace curs.Models.RoomType
{
    public class RoomTypeStatistic
    {
        public string Type { get; set; }
        public int Count { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace curs.Models.Room
{
    public class Room : IEquatable<Room>
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(10), MinLength(1)]
        public string Number { get; set; }

        [Required]
        public bool IsFree { get; set; }

        [Required]
        public virtual RoomType.RoomType RoomType { get; set; } 

        public virtual List<Request.Request> Requests { get; set; }

        public bool Equals(Room other)
        {
            return this.Id == other.Id;
        }
    }
}
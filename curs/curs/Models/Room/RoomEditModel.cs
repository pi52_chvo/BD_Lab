﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace curs.Models.Room
{
    public class RoomEditModel
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(10), MinLength(1)]
        public string Number { get; set; }

        [Required]
        public int RoomTypeId { get; set; }

        [Display(Name = "Room types")]
        public SelectList RoomTypes { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace curs.Models.Room
{
    public class RoomSearchOption
    {
        public string Number { get; set; }

        public bool? IsFree { get; set; }

        public string RoomTypeIds { get; set; }

        public string Price { get; set; }
    }
}
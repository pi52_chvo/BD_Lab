﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace curs.Models.Identity
{
    public class CreateWorker
    {
        [Required(ErrorMessage = "Поле должно быть заполнено")]
        [Display(Name = "Имья")]
        [DataType(DataType.Text)]
        [StringLength(maximumLength: 20, MinimumLength = 3, ErrorMessage = "Длина имени 2 - 20 символов")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Поле должно быть заполнено")]
        [Display(Name = "Фамилия")]
        [DataType(DataType.Text)]
        [StringLength(maximumLength: 20, MinimumLength = 3, ErrorMessage = "Длина фамилии 2 - 20 символов")]
        public string Surname { get; set; }

        [Required(ErrorMessage = "Поле должно быть заполнено")]
        [Display(Name = "Телефон")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"(\+380([1-9])\d{8})", ErrorMessage = "Wrong mobile")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Поле должно быть заполнено")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessage = "Поле должно быть заполнено")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Поле должно быть заполнено")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "Пароли не совпадают")]
        [DataType(DataType.Password)]
        public string PasswordConfirm { get; set; }

        [Required]
        public string[] RoleIds { get; set; }

        [Display(Name = "Roles")]
        public SelectList Roles { get; set; }
    }
}
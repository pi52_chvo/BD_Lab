﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace curs.Models.Service
{
    public class Service
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(15), MinLength(2)]
        public string Title { get; set; }

        [Required]
        public double Price { get; set; }

        public virtual List<OrderedService> OrderedServices { get; set; }
    }
}
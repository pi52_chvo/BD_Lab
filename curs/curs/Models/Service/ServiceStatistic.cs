﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace curs.Models.Service
{
    public class ServiceStatistic
    {
        public string Title { get; set; }
        public int Count { get; set; }
    }
}
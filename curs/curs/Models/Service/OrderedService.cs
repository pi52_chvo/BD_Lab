﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace curs.Models.Service
{
    public class OrderedService : IEquatable<OrderedService>
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(15), MinLength(2)]
        public string Title { get; set; }

        [Required]
        public double? Price { get; set; }

        public virtual Service Service { get; set; }

        public virtual List<Request.Request> Requests { get; set; }

        public bool Equals(OrderedService other)
        {
            return this.Id == other.Id;
        }
    }
}
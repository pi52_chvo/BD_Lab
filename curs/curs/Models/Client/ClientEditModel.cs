﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace curs.Models.Client
{
    public class ClientEditModel
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(15)]
        public string Name { get; set; }

        [Required]
        [MaxLength(15)]
        public string Surname { get; set; }

        [Required]
        [MaxLength(13)]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"(\+380[1-9][0-9]{8})", ErrorMessage = "Wrong mobile")]
        public string Phonenumber { get; set; }

        [Required]
        [MaxLength(50)]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        public string Index { get; set; }

        [Required]
        public string Locality { get; set; }

        [Required]
        public string Country { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace curs.Models.Client
{
    public class ClientSearchOption : Worker.WorkerSearchOption
    {
        public string Country { get; set; }
        public string Address { get; set; }
        public string Locality { get; set; }
        public string Index { get; set; }
    }
}
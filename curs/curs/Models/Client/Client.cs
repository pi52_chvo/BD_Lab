﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace curs.Models.Client
{
    public class Client
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string Phonenumber { get; set; }

        public string Email { get; set; }

        public string Address { get; set; }

        public string Index { get; set; }

        public string Locality { get; set; }

        public string Country { get; set; }

        public virtual List<Request.Request> Requests { get; set; }
    }
}
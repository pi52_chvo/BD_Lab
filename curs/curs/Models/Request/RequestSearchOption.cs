﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace curs.Models.Request
{
    public class RequestSearchOption
    {
        public string ArrivalDate { get; set; }

        public string DepartureDate { get; set; }

        public string ReservationDate { get; set; }

        public int? Adult { get; set; }

        public int? Child { get; set; }

        public string ClientEmail { get; set; }

        public string WorkerEmail { get; set; }

        public string Price { get; set; }

        public string RoomTypeIds { get; set; }
    }
}
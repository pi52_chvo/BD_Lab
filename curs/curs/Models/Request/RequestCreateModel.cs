﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace curs.Models.Request
{
    public class RequestCreateModel
    {
        public Client.ClientCreateModel Client { get; set; }
        
        public int[] RoomIds { get; set; }

        public int RoomTypeId { get; set; }

        [Display(Name = "Rooms")]
        public SelectList AllowedRooms { get; set; }

        [Display(Name = "Room types")]
        public SelectList AllowedRoomTypes { get; set; }

        public int[] ServiceIds { get; set; }

        [Display(Name = "Services")]
        public SelectList AllowedServices { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime ArrivalDate { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime DepartureDate { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime ReservationDate { get; set; }

        [Required]
        [UIHint("Number")]
        public int AdultCount { get; set; }

        [UIHint("Number")]
        public int? ChildCount { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace curs.Models.Request
{
    public class RequestStatisticModel
    {
        public double TotalSpent { get; set; }
        public int TotalClients { get; set; }
        public List<Models.RoomType.RoomType> PopularRoomTypes { get; set; } 
        public List<Models.Service.Service> PopularServices { get; set; }
    }
}
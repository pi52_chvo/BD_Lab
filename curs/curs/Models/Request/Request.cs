﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using curs.Models.Client;
using curs.Models.RoomType;

namespace curs.Models.Request
{
    public class Request
    {
        public int Id { get; set; }

        public virtual Client.Client Client { get; set; }

        public virtual Worker.Worker Worker { get; set; }

        public virtual List<Room.Room> Rooms { get; set; }

        public virtual List<OrderedRoomType> OrderedRoomTypes { get; set; }

        public virtual List<Service.OrderedService> Services { get; set; }

        [Required]
        public DateTime ArrivalDate { get; set; }

        [Required]
        public DateTime DepartureDate { get; set; }

        [Required]
        public DateTime ReservationDate { get; set; }

        public int? AdultCount { get; set; }

        public int? ChildCount { get; set; }
    }
}
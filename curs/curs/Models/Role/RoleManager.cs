﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace curs.Models.Role
{
    public class HotelRoleManager : RoleManager<HotelRole>
    {
        public HotelRoleManager(RoleStore<HotelRole> store)
                : base(store)
        { }

        public static HotelRoleManager Create(IdentityFactoryOptions<HotelRoleManager> options, IOwinContext context)
        {
            return new HotelRoleManager(new RoleStore<HotelRole>(context.Get<HotelContext>()));
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace curs.Models.Role
{
    public class CreateRole
    {
        [Required]
        public string Name { get; set; }
    }
}
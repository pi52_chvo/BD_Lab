﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace curs.Models.Role
{
    public class EditRole
    {
        [Required]
        public string Id { get; set; }

        [Required]
        public string Name { get; set; }
    }
}
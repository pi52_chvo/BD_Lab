﻿using curs.Models.Role;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace curs.Models.Worker
{
    public class Worker : IdentityUser
    {

        public string Name { get; set; }

        public string Surname { get; set; }

        public virtual List<Request.Request> Requests { get; set; }
    }
}
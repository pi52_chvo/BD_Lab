﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace curs.Models.Worker
{
    public class WorkerManager : UserManager<Worker>
    {
        public WorkerManager(IUserStore<Worker> store) : base(store)
        {
        }

        public static WorkerManager Create(IdentityFactoryOptions<WorkerManager> options, IOwinContext context)
        {
            HotelContext db = context.Get<HotelContext>();
            WorkerManager manager = new WorkerManager(new UserStore<Worker>(db));
            return manager;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace curs.Models.Worker
{
    public class WorkerEditModel
    {
        public string Id { get; set; }
        [Required]
        public string Name { get; set; }

        [Required]
        public string Surname { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"(\+380[1-9][0-9]{8})", ErrorMessage = "Wrong mobile")]
        public string Phone { get; set; }

        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }

        [Required]
        public string[] RoleIds { get; set; }

        [Display(Name = "Roles")]
        public System.Web.Mvc.SelectList Roles { get; set; }
    }
}
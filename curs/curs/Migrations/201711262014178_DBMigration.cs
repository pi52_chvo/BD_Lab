namespace curs.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DBMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Clients",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Surname = c.String(nullable: false),
                        Phonenumber = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        Address = c.String(nullable: false),
                        Index = c.String(nullable: false),
                        Locality = c.String(nullable: false),
                        Country = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Requests",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ArrivalDate = c.DateTime(nullable: false),
                        DepartureDate = c.DateTime(nullable: false),
                        ReservationDate = c.DateTime(nullable: false),
                        AdultCount = c.Int(nullable: false, defaultValue: 1),
                        ChildCount = c.Int(),
                        Client_Id = c.Int(nullable: false),
                        Worker_Id = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Clients", t => t.Client_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.Worker_Id)
                .Index(t => t.Client_Id)
                .Index(t => t.Worker_Id);
            
            CreateTable(
                "dbo.OrderedRoomTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 15),
                        Price = c.Double(nullable: false),
                        RoomType_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RoomTypes", t => t.RoomType_Id)
                .Index(t => t.RoomType_Id);
            
            CreateTable(
                "dbo.RoomTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 15),
                        Price = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Rooms",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Number = c.String(nullable: false, maxLength: 10),
                        IsFree = c.Boolean(nullable: false),
                        RoomType_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RoomTypes", t => t.RoomType_Id, cascadeDelete: true)
                .Index(t => t.RoomType_Id);
            
            CreateTable(
                "dbo.OrderedServices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 15),
                        Price = c.Double(nullable: false),
                        Service_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Services", t => t.Service_Id)
                .Index(t => t.Service_Id);
            
            CreateTable(
                "dbo.Services",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 15),
                        Price = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(),
                        Surname = c.String(),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.OrderedRoomTypeRequests",
                c => new
                    {
                        OrderedRoomType_Id = c.Int(nullable: false),
                        Request_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.OrderedRoomType_Id, t.Request_Id })
                .ForeignKey("dbo.OrderedRoomTypes", t => t.OrderedRoomType_Id, cascadeDelete: true)
                .ForeignKey("dbo.Requests", t => t.Request_Id, cascadeDelete: true)
                .Index(t => t.OrderedRoomType_Id)
                .Index(t => t.Request_Id);
            
            CreateTable(
                "dbo.RoomRequests",
                c => new
                    {
                        Room_Id = c.Int(nullable: false),
                        Request_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Room_Id, t.Request_Id })
                .ForeignKey("dbo.Rooms", t => t.Room_Id, cascadeDelete: true)
                .ForeignKey("dbo.Requests", t => t.Request_Id, cascadeDelete: true)
                .Index(t => t.Room_Id)
                .Index(t => t.Request_Id);
            
            CreateTable(
                "dbo.OrderedServiceRequests",
                c => new
                    {
                        OrderedService_Id = c.Int(nullable: false),
                        Request_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.OrderedService_Id, t.Request_Id })
                .ForeignKey("dbo.OrderedServices", t => t.OrderedService_Id, cascadeDelete: true)
                .ForeignKey("dbo.Requests", t => t.Request_Id, cascadeDelete: true)
                .Index(t => t.OrderedService_Id)
                .Index(t => t.Request_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Requests", "Worker_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderedServices", "Service_Id", "dbo.Services");
            DropForeignKey("dbo.OrderedServiceRequests", "Request_Id", "dbo.Requests");
            DropForeignKey("dbo.OrderedServiceRequests", "OrderedService_Id", "dbo.OrderedServices");
            DropForeignKey("dbo.Rooms", "RoomType_Id", "dbo.RoomTypes");
            DropForeignKey("dbo.RoomRequests", "Request_Id", "dbo.Requests");
            DropForeignKey("dbo.RoomRequests", "Room_Id", "dbo.Rooms");
            DropForeignKey("dbo.OrderedRoomTypes", "RoomType_Id", "dbo.RoomTypes");
            DropForeignKey("dbo.OrderedRoomTypeRequests", "Request_Id", "dbo.Requests");
            DropForeignKey("dbo.OrderedRoomTypeRequests", "OrderedRoomType_Id", "dbo.OrderedRoomTypes");
            DropForeignKey("dbo.Requests", "Client_Id", "dbo.Clients");
            DropIndex("dbo.OrderedServiceRequests", new[] { "Request_Id" });
            DropIndex("dbo.OrderedServiceRequests", new[] { "OrderedService_Id" });
            DropIndex("dbo.RoomRequests", new[] { "Request_Id" });
            DropIndex("dbo.RoomRequests", new[] { "Room_Id" });
            DropIndex("dbo.OrderedRoomTypeRequests", new[] { "Request_Id" });
            DropIndex("dbo.OrderedRoomTypeRequests", new[] { "OrderedRoomType_Id" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.OrderedServices", new[] { "Service_Id" });
            DropIndex("dbo.Rooms", new[] { "RoomType_Id" });
            DropIndex("dbo.OrderedRoomTypes", new[] { "RoomType_Id" });
            DropIndex("dbo.Requests", new[] { "Worker_Id" });
            DropIndex("dbo.Requests", new[] { "Client_Id" });
            DropTable("dbo.OrderedServiceRequests");
            DropTable("dbo.RoomRequests");
            DropTable("dbo.OrderedRoomTypeRequests");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Services");
            DropTable("dbo.OrderedServices");
            DropTable("dbo.Rooms");
            DropTable("dbo.RoomTypes");
            DropTable("dbo.OrderedRoomTypes");
            DropTable("dbo.Requests");
            DropTable("dbo.Clients");
        }
    }
}

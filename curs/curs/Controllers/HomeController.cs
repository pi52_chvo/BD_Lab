﻿using curs.Repositoryes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Rotativa;
using System.IO;

namespace curs.Controllers
{
    public class HomeController : Controller
    {

        private IUnitOfWork db;

        public HomeController(IUnitOfWork db)
        {
            this.db = db;
        }

        public FileResult Backups()
        {
            string directoryPath = Server.MapPath("") + "\\backup";
            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }
            var backupFileResult = db.Database.ExecuteSqlCommand("BACKUP DATABASE cursova TO DISK = '" + directoryPath + "\\cursovaBackup.bak ' WITH NORECOVERY");
            using (MemoryStream stream = new MemoryStream())
            {
                using (FileStream fileRead = new FileStream(directoryPath + "\\cursovaBackup.bak", FileMode.Open, FileAccess.Read))
                {
                    byte[] bytes = new byte[fileRead.Length];
                    int numBytesToRead = (int)fileRead.Length;
                    int numBytesRead = 0;
                    while (numBytesToRead > 0)
                    {
                        int n = fileRead.Read(bytes, numBytesRead, numBytesToRead);
                        if (n == 0)
                            break;

                        numBytesRead += n;
                        numBytesToRead -= n;
                    }
                    numBytesToRead = bytes.Length;
                    var contentType = MimeMapping.GetMimeMapping(directoryPath + "\\cursovaBackup.bak");
                    return File(bytes, contentType, "backup.bak");
                }
            }
        }

        public ActionResult GetStatistics()
        {
            return new ActionAsPdf("GetStatistics");
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
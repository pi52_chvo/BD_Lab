﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using curs;
using curs.Models.Service;
using curs.Repositoryes;
using System.IO;
using CsvHelper;

namespace curs.Controllers
{
    public class ServicesController : Controller
    {
        private IUnitOfWork db;

        public ServicesController(IUnitOfWork db)
        {
            this.db = db;
        }

        [Authorize]
        public ActionResult Index()
        {
            return View(db.Services.Get());
        }

        [Authorize(Roles = "mainadministrator")]
        public ActionResult Create()
        {
            return View();
        }

        [Authorize(Roles = "mainadministrator")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ServiceCreateModel serviceCreate)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Service service = new Service
                    {
                        Title = serviceCreate.Title,
                        Price = serviceCreate.Price
                    };
                    db.Services.Save(service);
                    db.Save();
                    return RedirectToAction("Index");
                }
                return View(serviceCreate);
            }
            catch (Exception ex)
            {
                ErrorViewModel error = new ErrorViewModel
                {
                    Code = "",
                    Message = ex.Message
                };
                return View("Error", error);
            }
        }

        [Authorize(Roles = "mainadministrator")]
        public ActionResult Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    ErrorViewModel error = new ErrorViewModel
                    {
                        Code = HttpStatusCode.BadRequest.ToString(),
                        Message = "Can not find this page"
                    };
                    return View("Error", error);
                }
                Service service = db.Services.GetById((int)id);
                if (service == null)
                {
                    ErrorViewModel error = new ErrorViewModel
                    {
                        Code = HttpStatusCode.NotFound.ToString(),
                        Message = "Service doesn't exist"
                    };
                    return View("Error", error);
                }
                ServiceEditModel serviceEdit = new ServiceEditModel
                {
                    Id = service.Id,
                    Title = service.Title,
                    Price = service.Price
                };
                return View(serviceEdit);
            }
            catch (Exception ex)
            {
                ErrorViewModel error = new ErrorViewModel
                {
                    Code = "",
                    Message = ex.Message
                };
                return View("Error", error);
            }
        }

        [Authorize(Roles = "mainadministrator")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ServiceEditModel serviceEdit)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Service service = db.Services.GetById(serviceEdit.Id);
                    service.Title = serviceEdit.Title;
                    service.Price = serviceEdit.Price;
                    db.Services.Update(service);
                    db.Save();
                    return RedirectToAction("Index");
                }
                return View(serviceEdit);
            }
            catch (Exception ex)
            {
                ErrorViewModel error = new ErrorViewModel
                {
                    Code = "",
                    Message = ex.Message
                };
                return View("Error", error);
            }
        }

        [Authorize(Roles = "mainadministrator")]
        public ActionResult Delete(int? id)
        {
            try
            {
                if (id == null)
                {
                    ErrorViewModel error = new ErrorViewModel
                    {
                        Code = HttpStatusCode.BadRequest.ToString(),
                        Message = "Can not find this page"
                    };
                    return View("Error", error);
                }
                Service service = db.Services.GetById((int)id);
                if (service == null)
                {
                    ErrorViewModel error = new ErrorViewModel
                    {
                        Code = HttpStatusCode.NotFound.ToString(),
                        Message = "Service doesn't exist"
                    };
                    return View("Error", error);
                }
                return View(service);
            }
            catch (Exception ex)
            {
                ErrorViewModel error = new ErrorViewModel
                {
                    Code = "",
                    Message = ex.Message
                };
                return View("Error", error);
            }
        }

        [Authorize(Roles = "mainadministrator")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Service service = db.Services.GetById(id);
                db.Services.Delete(service);
                db.Save();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ErrorViewModel error = new ErrorViewModel
                {
                    Code = "",
                    Message = ex.Message
                };
                return View("Error", error);
            }
        }

        public JsonResult SearchByOption(ServiceSearchOption option)
        {
            var result = (db.Services as ServiceRepository).Search(option).Select(s => new
            {
                Id = s.Id,
                Title = s.Title,
                Price = s.Price
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetOrdered(string field, string orderType)
        {
            var result = (db.Services as ServiceRepository).Order(field, orderType).Select(s => new
            {
                Id = s.Id,
                Title = s.Title,
                Price = s.Price
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public FileResult Export()
        {
            byte[] result = (db.Services as ServiceRepository).CsvToMemory();
            return File(result, "text/csv", "Services.csv");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

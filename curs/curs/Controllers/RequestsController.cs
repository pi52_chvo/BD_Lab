﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using curs;
using curs.Models.Request;
using curs.Repositoryes;
using curs.Models.Room;
using curs.Models.Client;
using curs.Models.Worker;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using curs.Models.Service;
using System.Data.SqlClient;
using curs.Models.RoomType;
using Rotativa;

namespace curs.Controllers
{
    public class RequestsController : Controller
    {
        private IUnitOfWork db;

        private WorkerManager workerManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<WorkerManager>();
            }
        }

        public RequestsController(IUnitOfWork db)
        {
            this.db = db;
        }

        [Authorize]
        public ActionResult Index()
        {
            ViewBag.RoomTypes = db.RoomTypes.Get();
            return View(db.Requests.Get());
        }

        [Authorize]
        public ActionResult Details(int? id, string print)
        {
            try
            {
                if (id == null)
                {
                    ErrorViewModel error = new ErrorViewModel
                    {
                        Code = HttpStatusCode.BadRequest.ToString(),
                        Message = "Can not find this page"
                    };
                    return View("Error", error);
                }
                Request request = db.Requests.GetById((int)id);
                if (request == null)
                {
                    ErrorViewModel error = new ErrorViewModel
                    {
                        Code = HttpStatusCode.NotFound.ToString(),
                        Message = "Reservation doesn't exist"
                    };
                    return View("Error", error);
                }
                if (!string.IsNullOrEmpty(print))
                {
                    RedirectToAction("Print", new { id = id });
                }
                return View(request);
            }
            catch (Exception ex)
            {
                ErrorViewModel error = new ErrorViewModel
                {
                    Code = "",
                    Message = ex.Message
                };
                return View("Error", error);
            }
        }

        [Authorize(Roles = "administrator")]
        public ActionResult Print(int id)
        {
            try
            {
                Request request = db.Requests.GetById(id);
                return new ViewAsPdf(request);
            }
            catch (Exception ex)
            {
                ErrorViewModel error = new ErrorViewModel
                {
                    Code = "",
                    Message = ex.Message
                };
                return View("Error", error);
            }
        }

        [Authorize(Roles = "administrator")]
        public ActionResult Create()
        {
            try
            {
                RequestCreateModel requestCreate = new RequestCreateModel
                {
                    AllowedRoomTypes = (db.RoomTypes as RoomTypeRepository).GetDropdownList(),
                    AllowedRooms = new SelectList(new List<Room>()),
                    AllowedServices = (db.Services as ServiceRepository).GetDropdownList()
                };
                return View(requestCreate);
            }
            catch (Exception ex)
            {
                ErrorViewModel error = new ErrorViewModel
                {
                    Code = "",
                    Message = ex.Message
                };
                return View("Error", error);
            }
        }

        [Authorize(Roles = "administrator")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(RequestCreateModel requestCreate)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (requestCreate.RoomIds == null || requestCreate.RoomIds.Length == 0)
                    {
                        ModelState.AddModelError("", "Room must be selected");
                        requestCreate.AllowedRoomTypes = (db.RoomTypes as RoomTypeRepository).GetDropdownList();
                        requestCreate.AllowedRooms = new SelectList(new List<Room>());
                        requestCreate.AllowedServices = (db.Services as ServiceRepository).GetDropdownList();
                        return View(requestCreate);
                    }
                    if (requestCreate.DepartureDate < requestCreate.ArrivalDate)
                    {
                        ModelState.AddModelError("", "Incorrect dates range");
                        requestCreate.AllowedRoomTypes = (db.RoomTypes as RoomTypeRepository).GetDropdownList();
                        requestCreate.AllowedRooms = new SelectList(new List<Room>());
                        requestCreate.AllowedServices = (db.Services as ServiceRepository).GetDropdownList();
                        return View(requestCreate);
                    }
                    using (var transaction = db.Database.BeginTransaction())
                    {
                        try
                        {
                            Client client = new Client
                            {
                                Name = requestCreate.Client.Name,
                                Surname = requestCreate.Client.Surname,
                                Phonenumber = requestCreate.Client.Phonenumber,
                                Email = requestCreate.Client.Email,
                                Country = requestCreate.Client.Country,
                                Locality = requestCreate.Client.Locality,
                                Address = requestCreate.Client.Address,
                                Index = requestCreate.Client.Index
                            };
                            db.Clients.Save(client);
                            db.Save();
                            Worker worker = db.Workers.Get(w => w.Id == User.Identity.GetUserId()).FirstOrDefault();
                            List<Room> rooms = db.Rooms.Get(r => requestCreate.RoomIds.Contains(r.Id)).ToList();
                            List<OrderedService> orderedService = new List<OrderedService>();
                            if (requestCreate.ServiceIds != null && requestCreate.ServiceIds.Length > 0)
                            {
                                List<Service> services = db.Services.Get(s => requestCreate.ServiceIds.Contains(s.Id)).ToList();
                                foreach (var item in services)
                                {
                                    OrderedService s = db.OrderedServices.Get(os => os.Title == item.Title && os.Price == item.Price && os.Service.Id == item.Id).FirstOrDefault();
                                    if (s == null)
                                    {
                                        s = new OrderedService
                                        {
                                            Title = item.Title,
                                            Price = item.Price,
                                            Service = item
                                        };
                                        db.OrderedServices.Save(s);
                                    }
                                    orderedService.Add(s);
                                }
                            }
                            List<OrderedRoomType> orderedRoomTypes = new List<OrderedRoomType>();
                            foreach (var item in rooms)
                            {
                                OrderedRoomType r = db.OrderedRoomTypes.Get(ort => ort.Price == item.RoomType.Price && ort.RoomType.Title == item.RoomType.Title && ort.RoomType.Id == item.RoomType.Id).FirstOrDefault();
                                if (r == null)
                                {
                                    r = new OrderedRoomType
                                    {
                                        Title = item.RoomType.Title,
                                        Price = item.RoomType.Price,
                                        RoomType = item.RoomType
                                    };
                                    db.OrderedRoomTypes.Save(r);
                                }
                                orderedRoomTypes.Add(r);
                            }
                            DateTime arrival = new DateTime(requestCreate.ArrivalDate.Year, requestCreate.ArrivalDate.Month, requestCreate.ArrivalDate.Day, requestCreate.ArrivalDate.Hour, 0, 0);
                            DateTime departure = new DateTime(requestCreate.DepartureDate.Year, requestCreate.DepartureDate.Month, requestCreate.DepartureDate.Day, requestCreate.DepartureDate.Hour, 0, 0);
                            Request request = new Request
                            {
                                Client = client,
                                Worker = worker,
                                Rooms = rooms,
                                Services = null,
                                ArrivalDate = requestCreate.ArrivalDate,
                                OrderedRoomTypes = orderedRoomTypes,
                                DepartureDate = requestCreate.DepartureDate,
                                ReservationDate = DateTime.Now,
                                AdultCount = requestCreate.AdultCount,
                                ChildCount = requestCreate.ChildCount
                            };
                            if (orderedService.Count > 0)
                                request.Services = orderedService;
                            db.Requests.Save(request);
                            db.Save();
                            transaction.Commit();
                            return RedirectToAction("Details", new { id = request.Id });
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            return new HttpStatusCodeResult(500);
                        }
                    }
                }
                requestCreate.AllowedRoomTypes = (db.RoomTypes as RoomTypeRepository).GetDropdownList();
                requestCreate.AllowedRooms = new SelectList(new List<Room>());
                requestCreate.AllowedServices = (db.Services as ServiceRepository).GetDropdownList();
                return View(requestCreate);
            }
            catch (Exception ex)
            {
                ErrorViewModel error = new ErrorViewModel
                {
                    Code = "",
                    Message = ex.Message
                };
                return View("Error", error);
            }
        }

        [Authorize(Roles = "administrator")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Request request = db.Requests.GetById((int)id);
            if (request == null)
            {
                return HttpNotFound();
            }
            RequestEditModel editRequest = new RequestEditModel
            {
                Id = request.Id,
                ArrivalDate = request.ArrivalDate,
                DepartureDate = request.DepartureDate,
                ReservationDate = request.ReservationDate,
                AdultCount = (int)request.AdultCount,
                ChildCount = request.ChildCount,
                AllowedRooms = (db.Rooms as RoomRepository).GetDropdownList(request.Rooms.Select(r => r.Id).ToArray()),
                AllowedServices = (db.Services as ServiceRepository).GetDropdownList(request.Services.Select(s => s.Service.Id).ToArray()),
                AllowedRoomTypes = (db.RoomTypes as RoomTypeRepository).GetDropdownList(),
                RoomIds = request.Rooms.Select(r => r.Id).ToArray(),
                ServiceIds = request.Services.Select(s => s.Service.Id).ToArray()
            };
            return View(editRequest);
        }

        [Authorize(Roles = "administrator")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(RequestEditModel requestCreate)
        {
            if (ModelState.IsValid)
            {
                List<Room> rooms = db.Rooms.Get(r => requestCreate.RoomIds.Contains(r.Id)).ToList();
                List<OrderedService> orderedService = new List<OrderedService>();
                if (requestCreate.ServiceIds != null && requestCreate.ServiceIds.Length > 0)
                {
                    List<Service> services = db.Services.Get(s => requestCreate.ServiceIds.Contains(s.Id)).ToList();
                    foreach (var item in services)
                    {
                        OrderedService s = db.OrderedServices.Get(os => os.Title == item.Title && os.Price == item.Price && os.Service.Id == item.Id).FirstOrDefault();
                        if (s == null)
                        {
                            s = new OrderedService
                            {
                                Title = item.Title,
                                Price = item.Price,
                                Service = item
                            };
                            db.OrderedServices.Save(s);
                        }
                        orderedService.Add(s);
                    }
                }
                List<OrderedRoomType> orderedRoomTypes = new List<OrderedRoomType>();
                foreach (var item in rooms)
                {
                    OrderedRoomType r = db.OrderedRoomTypes.Get(ort => ort.Price == item.RoomType.Price && ort.RoomType.Title == item.RoomType.Title && ort.RoomType.Id == item.RoomType.Id).FirstOrDefault();
                    if (r == null)
                    {
                        r = new OrderedRoomType
                        {
                            Title = item.RoomType.Title,
                            Price = item.RoomType.Price,
                            RoomType = item.RoomType
                        };
                        db.OrderedRoomTypes.Save(r);
                    }
                    orderedRoomTypes.Add(r);
                }
                Request request = db.Requests.GetById(requestCreate.Id);
                request.ArrivalDate = requestCreate.ArrivalDate;
                request.DepartureDate = request.DepartureDate;
                request.AdultCount = requestCreate.AdultCount;
                request.ChildCount = requestCreate.ChildCount;
                if (rooms != null && rooms.Count > 0)
                {
                    foreach (var item in request.Rooms.ToList())
                    {
                        if (!rooms.Select(ort => ort.Id).Contains(item.Id))
                        {
                            request.Rooms.Remove(item);
                        }
                    }
                    foreach (var item in rooms)
                    {
                        if (!request.Rooms.Select(r => r.Id).Contains(item.Id))
                        {
                            request.Rooms.Add(item);
                        }
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Room must be selected");
                    requestCreate.AllowedRooms = (db.Rooms as RoomRepository).GetDropdownList(db.Requests.GetById(requestCreate.Id).Rooms.Select(r => r.Id).ToArray());
                    requestCreate.AllowedServices = (db.Services as ServiceRepository).GetDropdownList(db.Requests.GetById(requestCreate.Id).Services.Select(s => s.Service.Id).ToArray());
                    requestCreate.AllowedRoomTypes = (db.RoomTypes as RoomTypeRepository).GetDropdownList();
                    return View(requestCreate);

                }
                foreach (var item in request.OrderedRoomTypes.ToList())
                {
                    if (!orderedRoomTypes.Select(ort => ort.Id).Contains(item.Id))
                    {
                        request.OrderedRoomTypes.Remove(item);
                    }
                }
                foreach (var item in orderedRoomTypes)
                {
                    if (!request.OrderedRoomTypes.Select(ort => ort.Id).Contains(item.Id))
                    {
                        request.OrderedRoomTypes.Add(item);
                    }
                }
                if (orderedService.Count > 0)
                {
                    foreach (var item in request.Services.ToList())
                    {
                        if (!orderedService.Select(ort => ort.Id).Contains(item.Id))
                        {
                            request.Services.Remove(item);
                        }
                    }
                    foreach (var item in orderedService)
                    {
                        if (!request.Services.Select(r => r.Id).ToArray().Contains(item.Id))
                        {
                            request.Services.Add(item);
                        }
                    }
                }
                else
                {
                    request.Services.RemoveAll(r => r.Id > 0);
                }
                db.Requests.Update(request);
                db.Save();
                return RedirectToAction("Details", new { id = request.Id });
            }
            requestCreate.AllowedRooms = (db.Rooms as RoomRepository).GetDropdownList(db.Requests.GetById(requestCreate.Id).Rooms.Select(r => r.Id).ToArray());
            requestCreate.AllowedServices = (db.Services as ServiceRepository).GetDropdownList(db.Requests.GetById(requestCreate.Id).Services.Select(s => s.Service.Id).ToArray());
            requestCreate.AllowedRoomTypes = (db.RoomTypes as RoomTypeRepository).GetDropdownList();
            return View(requestCreate);
        }
        public ActionResult Delete(int? id)
        {
            try
            {
                if (id == null)
                {
                    ErrorViewModel error = new ErrorViewModel
                    {
                        Code = HttpStatusCode.BadRequest.ToString(),
                        Message = "Can not find this page"
                    };
                    return View("Error", error);
                }
                Request request = db.Requests.GetById((int)id);
                if (request == null)
                {
                    ErrorViewModel error = new ErrorViewModel
                    {
                        Code = HttpStatusCode.NotFound.ToString(),
                        Message = "Reservation doesn't exist"
                    };
                    return View("Error", error);
                }
                return View(request);
            }
            catch (Exception ex)
            {
                ErrorViewModel error = new ErrorViewModel
                {
                    Code = "",
                    Message = ex.Message
                };
                return View("Error", error);
            }
        }

        [Authorize(Roles = "administrator")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Request request = db.Requests.GetById(id);
                Client client = db.Clients.GetById(request.Client.Id);
                db.Clients.Delete(client);
                db.Requests.Delete(request);
                db.Save();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ErrorViewModel error = new ErrorViewModel
                {
                    Code = "",
                    Message = ex.Message
                };
                return View("Error", error);
            }
        }

        public FileResult Export()
        {
            byte[] result = (db.Requests as RequestRepository).CsvToMemory();
            return File(result, "text/csv", "Requests.csv");
        }

        public ActionResult Statistic()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetStatistic(int dateType, string date)
        {
            StatisticViewModel statistic = (db.Requests as RequestRepository).Statistics(dateType, date);
            return Json(statistic, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public JsonResult IncomesByYear(int year)
        {
            SqlParameter yearParameter = new SqlParameter("@year", year);
            var result = db.Database.SqlQuery<JsonResultIncome>("sp_get_income @year", yearParameter);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ClientsByYear(int year)
        {
            SqlParameter yearParameter = new SqlParameter("@year", year);
            var result = db.Database.SqlQuery<JsonResultClients>("sp_get_clients_count @year", yearParameter);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SearchByOption(RequestSearchOption option)
        {
            var result = (db.Requests as RequestRepository).Search(option).Select(s => new
            {
                Id = s.Id,
                ArrivalDate = s.ArrivalDate.ToString("dd.MM.yyyy hh:mm:ss"),
                DepartureDate = s.DepartureDate.ToString("dd.MM.yyyy hh:mm:ss"),
                ReservationDate = s.ReservationDate.ToString("dd.MM.yyyy hh:mm:ss"),
                Client = s.Client.Surname,
                Worker = s.Worker.Surname,
                Total = ((s.DepartureDate.Day - s.ArrivalDate.Day) * (s.OrderedRoomTypes.Sum(rt => rt.Price) + s.Services.Sum(rt => rt.Price)))
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetOrdered(string field, string orderType)
        {
            var result = (db.Requests as RequestRepository).Order(field, orderType).Select(s => new
            {
                Id = s.Id,
                ArrivalDate = s.ArrivalDate.ToString("dd.MM.yyyy hh:mm:ss"),
                DepartureDate = s.DepartureDate.ToString("dd.MM.yyyy hh:mm:ss"),
                ReservationDate = s.ReservationDate.ToString("dd.MM.yyyy hh:mm:ss"),
                Client = s.Client.Surname,
                Worker = s.Worker.Surname,
                Total = ((s.DepartureDate.Day - s.ArrivalDate.Day) * (s.OrderedRoomTypes.Sum(rt => rt.Price) + s.Services.Sum(rt => rt.Price)))
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }

    public class JsonResultIncome
    {
        public int Month { get; set; }

        public double Spent { get; set; }
    }

    public class JsonResultClients
    {
        public int Month { get; set; }

        public int Count { get; set; }
    }

    public class StatisticViewModel
    {
        public double Spent { get; set; }
        public int Clients { get; set; }
        public List<RoomTypeStatistic> RoomTypes { get; set; }
        public List<RoomTypeStatistic> AllRoomTypes { get; set; }
        public List<ServiceStatistic> Services { get; set; }
        public List<ServiceStatistic> AllServices { get; set; }
    }

    public class StatisticModel
    {
        public DateType DateType { get; set; }
        public DateTime Date { get; set; }
    }

    public enum DateType
    {
        Day,
        Month,
        Year
    }
}

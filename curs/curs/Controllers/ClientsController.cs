﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using curs;
using curs.Models.Client;
using curs.Repositoryes;

namespace curs.Controllers
{
    public class ClientsController : Controller
    {
        private IUnitOfWork db;

        public ClientsController(IUnitOfWork db)
        {
            this.db = db;
        }

        [Authorize]
        public ActionResult Index()
        {
            return View(db.Clients.Get());
        }

        [Authorize]
        public ActionResult Details(int? id)
        {
            try
            {
                if (id == null)
                {
                    ErrorViewModel error = new ErrorViewModel
                    {
                        Code = HttpStatusCode.BadRequest.ToString(),
                        Message = "Can not find this page"
                    };
                    return View("Error", error);
                }
                Client client = db.Clients.GetById((int)id);
                if (client == null)
                {
                    ErrorViewModel error = new ErrorViewModel
                    {
                        Code = HttpStatusCode.NotFound.ToString(),
                        Message = "Client doesn't exist"
                    };
                    return View("Error", error);
                }
                return View(client);
            }
            catch (Exception ex)
            {
                ErrorViewModel error = new ErrorViewModel
                {
                    Code = "",
                    Message = ex.Message
                };
                return View("Error", error);
            }
        }

        [Authorize(Roles = "administrator")]
        public ActionResult Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    ErrorViewModel error = new ErrorViewModel
                    {
                        Code = HttpStatusCode.BadRequest.ToString(),
                        Message = "Can not find this page"
                    };
                    return View("Error", error);
                }
                Client client = db.Clients.GetById((int)id);
                if (client == null)
                {
                    ErrorViewModel error = new ErrorViewModel
                    {
                        Code = HttpStatusCode.NotFound.ToString(),
                        Message = "Client doesn't exist"
                    };
                    return View("Error", error);
                }
                ClientEditModel clientEdit = new ClientEditModel
                {
                    Id = client.Id,
                    Name = client.Name,
                    Surname = client.Surname,
                    Phonenumber = client.Phonenumber,
                    Email = client.Email,
                    Address = client.Address,
                    Index = client.Index,
                    Country = client.Country,
                    Locality = client.Locality
                };
                return View(clientEdit);
            }
            catch (Exception ex)
            {
                ErrorViewModel error = new ErrorViewModel
                {
                    Code = "",
                    Message = ex.Message
                };
                return View("Error", error);
            }
        }

        [Authorize(Roles = "administrator")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ClientEditModel clientEdit)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Client client = new Client
                    {
                        Id = clientEdit.Id,
                        Name = clientEdit.Name,
                        Surname = clientEdit.Surname,
                        Phonenumber = clientEdit.Phonenumber,
                        Email = clientEdit.Email,
                        Address = clientEdit.Address,
                        Index = clientEdit.Index,
                        Country = clientEdit.Country,
                        Locality = clientEdit.Locality
                    };
                    db.Clients.Update(client);
                    db.Save();
                    return RedirectToAction("Index");
                }
                return View(clientEdit);
            }
            catch (Exception ex)
            {
                ErrorViewModel error = new ErrorViewModel
                {
                    Code = "",
                    Message = ex.Message
                };
                return View("Error", error);
            }
        }

        [Authorize(Roles = "administrator")]
        public ActionResult Delete(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Client client = db.Clients.GetById((int)id);
                if (client == null)
                {
                    return HttpNotFound();
                }
                return View(client);
            }
            catch (Exception ex)
            {
                ErrorViewModel error = new ErrorViewModel
                {
                    Code = "",
                    Message = ex.Message
                };
                return View("Error", error);
            }
        }

        [Authorize(Roles = "administrator")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Client client = db.Clients.GetById(id);
                db.Clients.Delete(client);
                db.Save();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ErrorViewModel error = new ErrorViewModel
                {
                    Code = "",
                    Message = ex.Message
                };
                return View("Error", error);
            }
        }

        public FileResult Export()
        {
            byte[] result = (db.Clients as ClientRepository).CsvToMemory();
            return File(result, "text/csv", "Clients.csv");
        }

        public JsonResult SearchByOption(ClientSearchOption option)
        {
            var result = (db.Clients as ClientRepository).Search(option).Select(s => new
            {
                Id = s.Id,
                Name = s.Name,
                Surname = s.Surname,
                Email = s.Email,
                Phonenumber = s.Phonenumber,
                Country = s.Country
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetOrdered(string field, string orderType)
        {
            var result = (db.Clients as ClientRepository).Order(field, orderType).Select(s => new
            {
                Id = s.Id,
                Name = s.Name,
                Surname = s.Surname,
                Email = s.Email,
                Phonenumber = s.Phonenumber,
                Country = s.Country
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

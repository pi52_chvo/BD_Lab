﻿using curs.Models.Role;
using curs.Models.Worker;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace curs.Controllers
{
    public class RolesController : Controller
    {
        private WorkerManager workerManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<WorkerManager>();
            }
        }

        private HotelRoleManager RoleManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<HotelRoleManager>();
            }
        }

        public ActionResult Index()
        {
            return View(RoleManager.Roles);
        }

        public ActionResult Create()
        {
            Worker worker = workerManager.FindById(User.Identity.GetUserId());
            if(worker == null)
            {
                return RedirectToAction("index");
            }
                return View();
        }

        [HttpPost]
        public async Task<ActionResult> Create(CreateRole model)
        {
            if (ModelState.IsValid)
            {
                IdentityResult result = await RoleManager.CreateAsync(new HotelRole
                {
                    Name = model.Name
                });
                if (result.Succeeded)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("", "Что-то пошло не так");
                }
            }
            return View(model);
        }

        
        public ActionResult Edit(string id)
        {
            HotelRole role = RoleManager.FindById(id);
            if(role == null)
            {
                return new HttpStatusCodeResult(404);
            }
            EditRole roleEdit = new EditRole
            {
                Id = role.Id,
                Name = role.Name
            };
            return View(roleEdit);
        }

        [HttpPost]
        public ActionResult Edit(EditRole editRole)
        {
            if(ModelState.IsValid)
            {
                HotelRole role = new HotelRole
                {
                    Id = editRole.Id,
                    Name = editRole.Name
                };
                RoleManager.Update(role);
                return RedirectToAction("Index");
            }
            return View(editRole);
        }

    }
}
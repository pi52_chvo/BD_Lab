﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using curs;
using curs.Repositoryes;
using curs.Models.RoomType;

namespace curs.Controllers
{
    public class RoomTypesController : Controller
    {
        private IUnitOfWork db;

        public RoomTypesController(IUnitOfWork db)
        {
            this.db = db;
        }

        [Authorize]
        public ActionResult Index()
        {
            return View(db.RoomTypes.Get().ToList());
        }

        [Authorize(Roles = "mainadministrator")]
        public ActionResult Create()
        {
            return View();
        }

        [Authorize(Roles = "mainadministrator")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(RoomTypeCreateModel roomTypeCreate)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    RoomType roomType = new RoomType
                    {
                        Title = roomTypeCreate.Title,
                        Price = roomTypeCreate.Price
                    };
                    db.RoomTypes.Save(roomType);
                    db.Save();
                    return RedirectToAction("Index");
                }
                return View(roomTypeCreate);
            }
            catch (Exception ex)
            {
                ErrorViewModel error = new ErrorViewModel
                {
                    Code = "",
                    Message = ex.Message
                };
                return View("Error", error);
            }
        }

        [Authorize(Roles = "mainadministrator")]
        public ActionResult Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    ErrorViewModel error = new ErrorViewModel
                    {
                        Code = HttpStatusCode.BadRequest.ToString(),
                        Message = "Can not find this page"
                    };
                    return View("Error", error);
                }
                RoomType roomType = db.RoomTypes.GetById((int)id);
                if (roomType == null)
                {
                    ErrorViewModel error = new ErrorViewModel
                    {
                        Code = HttpStatusCode.NotFound.ToString(),
                        Message = "Room type doesn't exist"
                    };
                    return View("Error", error);
                }
                RoomTypeEditModel roomTypeEdit = new RoomTypeEditModel
                {
                    Id = roomType.Id,
                    Title = roomType.Title,
                    Price = roomType.Price
                };
                return View(roomTypeEdit);
            }
            catch (Exception ex)
            {
                ErrorViewModel error = new ErrorViewModel
                {
                    Code = "",
                    Message = ex.Message
                };
                return View("Error", error);
            }
        }

        [Authorize(Roles = "mainadministrator")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(RoomTypeEditModel roomTypeEdit)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    RoomType roomType = db.RoomTypes.GetById(roomTypeEdit.Id);
                    roomType.Title = roomTypeEdit.Title;
                    roomType.Price = roomTypeEdit.Price;
                    db.RoomTypes.Update(roomType);
                    db.Save();
                    return RedirectToAction("Index");
                }
                return View(roomTypeEdit);
            }
            catch (Exception ex)
            {
                ErrorViewModel error = new ErrorViewModel
                {
                    Code = "",
                    Message = ex.Message
                };
                return View("Error", error);
            }
        }

        [Authorize(Roles = "mainadministrator")]
        public ActionResult Delete(int? id)
        {
            try
            {
                if (id == null)
                {
                    ErrorViewModel error = new ErrorViewModel
                    {
                        Code = HttpStatusCode.BadRequest.ToString(),
                        Message = "Can not find this page"
                    };
                    return View("Error", error);
                }
                RoomType roomType = db.RoomTypes.GetById((int)id);
                if (roomType == null)
                {
                    ErrorViewModel error = new ErrorViewModel
                    {
                        Code = HttpStatusCode.NotFound.ToString(),
                        Message = "Room type doesn't exist"
                    };
                    return View("Error", error);
                }
                return View(roomType);
            }
            catch (Exception ex)
            {
                ErrorViewModel error = new ErrorViewModel
                {
                    Code = "",
                    Message = ex.Message
                };
                return View("Error", error);
            }
        }

        [Authorize(Roles = "mainadministrator")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                RoomType roomType = db.RoomTypes.GetById(id);
                db.RoomTypes.Delete(roomType);
                db.Save();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ErrorViewModel error = new ErrorViewModel
                {
                    Code = "",
                    Message = ex.Message
                };
                return View("Error", error);
            }
        }

        public JsonResult GetOrdered(string field, string orderType)
        {
            var result = (db.RoomTypes as RoomTypeRepository).Order(field, orderType).Select(s => new
            {
                Id = s.Id,
                Title = s.Title,
                Price = s.Price
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SearchByOption(RoomTypeSearchOption option)
        {
            var result = (db.RoomTypes as RoomTypeRepository).Search(option).Select(s => new
            {
                Id = s.Id,
                Title = s.Title,
                Price = s.Price
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public FileResult Export()
        {
            byte[] result = (db.RoomTypes as RoomTypeRepository).CsvToMemory();
            return File(result, "text/csv", "RoomTypes.csv");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

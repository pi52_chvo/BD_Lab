﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using curs;
using curs.Models.Worker;
using curs.Repositoryes;
using Microsoft.AspNet.Identity.Owin;
using curs.Models.Identity;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System.Security.Claims;
using curs.Models.Role;
using System.Threading.Tasks;

namespace curs.Controllers
{
    [Authorize(Roles = "mainadministrator")]
    public class WorkersController : Controller
    {
        private IUnitOfWork db;

        private WorkerManager workerManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<WorkerManager>();
            }
        }

        private HotelRoleManager RoleManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<HotelRoleManager>();
            }
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        public WorkersController(IUnitOfWork db)
        {
            this.db = db;
        }

        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                return View(db.Workers.Get().ToList());
            }
            else return RedirectToAction("Login");
        }

        public ActionResult Details(string id)
        {
            try
            {
                if (id == null)
                {
                    ErrorViewModel error = new ErrorViewModel
                    {
                        Code = HttpStatusCode.BadRequest.ToString(),
                        Message = "Can not find this page"
                    };
                    return View("Error", error);
                }
                Worker worker = workerManager.FindById(id);
                if (worker == null)
                {
                    ErrorViewModel error = new ErrorViewModel
                    {
                        Code = HttpStatusCode.NotFound.ToString(),
                        Message = "Worker doesn't exist"
                    };
                    return View("Error", error);
                }
                return View(worker);
            }
            catch (Exception ex)
            {
                ErrorViewModel error = new ErrorViewModel
                {
                    Code = "",
                    Message = ex.Message
                };
                return View("Error", error);
            }
        }

        public ActionResult Create()
        {
            try
            {
                CreateWorker createWorker = new CreateWorker
                {
                    Roles = new SelectList(RoleManager.Roles.Select(r => new { Select = false, Text = r.Name, Value = r.Id }), "Value", "Text")
                };
                return View(createWorker);
            }
            catch (Exception ex)
            {
                ErrorViewModel error = new ErrorViewModel
                {
                    Code = "",
                    Message = ex.Message
                };
                return View("Error", error);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateWorker newWorker)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Worker worker = new Worker
                    {
                        UserName = newWorker.Email,
                        Name = newWorker.Name,
                        Surname = newWorker.Surname,
                        PhoneNumber = newWorker.Phone,
                        Email = newWorker.Email
                    };
                    IdentityResult result = workerManager.Create(worker, newWorker.Password);
                    if (result.Succeeded)
                    {
                        foreach (var id in newWorker.RoleIds)
                        {
                            var role = RoleManager.FindById(id.ToString());
                            workerManager.AddToRole(worker.Id, role.Name);
                        }
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        foreach (string error in result.Errors)
                        {
                            ModelState.AddModelError("", error);
                        }
                    }
                }
                newWorker.Roles = new SelectList(RoleManager.Roles.Select(r => new { Select = false, Text = r.Name, Value = r.Id }), "Value", "Text");
                return View(newWorker);
            }
            catch (Exception ex)
            {
                ErrorViewModel error = new ErrorViewModel
                {
                    Code = "",
                    Message = ex.Message
                };
                return View("Error", error);
            }
        }

        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public ActionResult Login(LoginWorker loginWorker)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Worker worker = workerManager.Find(loginWorker.Email, loginWorker.Password);
                    if (worker == null)
                    {
                        ModelState.AddModelError("", "Неверный логин или пароль.");
                    }
                    else
                    {
                        ClaimsIdentity claim = workerManager.CreateIdentity(worker, DefaultAuthenticationTypes.ApplicationCookie);
                        AuthenticationManager.SignOut();
                        AuthenticationManager.SignIn(new AuthenticationProperties
                        {
                            IsPersistent = true
                        }, claim);
                        return RedirectToAction("Index", "Home");
                    }
                }
                return View(loginWorker);
            }
            catch (Exception ex)
            {
                ErrorViewModel error = new ErrorViewModel
                {
                    Code = "",
                    Message = ex.Message
                };
                return View("Error", error);
            }
        }

        public ActionResult Logout()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Login");
        }

        public ActionResult Edit(string id)
        {
            try
            {
                if (id == null)
                {
                    ErrorViewModel error = new ErrorViewModel
                    {
                        Code = HttpStatusCode.BadRequest.ToString(),
                        Message = "Can not find this page"
                    };
                    return View("Error", error);
                }
                Worker worker = workerManager.FindById(id);
                if (worker == null)
                {
                    ErrorViewModel error = new ErrorViewModel
                    {
                        Code = HttpStatusCode.NotFound.ToString(),
                        Message = "Worker doesn't exist"
                    };
                    return View("Error", error);
                }
                WorkerEditModel editWorker = new WorkerEditModel
                {
                    Id = worker.Id,
                    Name = worker.Name,
                    Surname = worker.Surname,
                    Email = worker.Email,
                    Phone = worker.PhoneNumber,
                    Password = "",
                    ConfirmPassword = "",
                    RoleIds = worker.Roles.Select(r => r.RoleId).ToArray()
                };
                List<SelectListItem> roles = new List<SelectListItem>();
                foreach (var r in RoleManager.Roles)
                {
                    var item = new SelectListItem
                    {
                        Selected = false,
                        Text = r.Name,
                        Value = r.Id
                    };
                    if (editWorker.RoleIds.Contains(r.Id))
                        item.Selected = true;
                    roles.Add(item);
                }
                editWorker.Roles = new SelectList(roles, "Value", "Text");
                return View(editWorker);
            }
            catch (Exception ex)
            {
                ErrorViewModel error = new ErrorViewModel
                {
                    Code = "",
                    Message = ex.Message
                };
                return View("Error", error);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(WorkerEditModel editWorker)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Worker worker = workerManager.FindById(editWorker.Id);
                    if (worker != null)
                    {
                        worker.Email = editWorker.Email;
                        IdentityResult validEmail = await workerManager.UserValidator.ValidateAsync(worker);
                        if (!validEmail.Succeeded)
                        {
                            ModelState.AddModelError("", "Некоректный Email");
                            return View(editWorker);
                        }
                        IdentityResult validPassword = null;
                        if (!string.IsNullOrEmpty(editWorker.Password))
                        {
                            validPassword = await workerManager.PasswordValidator.ValidateAsync(editWorker.Password);
                            if (validPassword.Succeeded)
                            {
                                worker.PasswordHash = workerManager.PasswordHasher.HashPassword(editWorker.Password);
                            }
                            else
                            {
                                ModelState.AddModelError("", "Некоректный Password");
                            }
                        }
                        if ((validEmail.Succeeded && validPassword == null) || (validPassword.Succeeded && validEmail.Succeeded && editWorker.Password != string.Empty))
                        {
                            var roless = RoleManager.Roles.ToList().Where(r => worker.Roles.ToList().Select(wr => wr.RoleId).ToArray().Contains(r.Id)).Select(r => r.Name).ToArray();
                            workerManager.RemoveFromRoles(worker.Id, roless);
                            worker.Email = editWorker.Email;
                            worker.Name = editWorker.Name;
                            worker.Surname = editWorker.Surname;
                            worker.UserName = worker.Email;
                            worker.PhoneNumber = editWorker.Phone;
                            foreach (var id in editWorker.RoleIds)
                            {
                                var role = RoleManager.FindById(id.ToString());
                                workerManager.AddToRole(worker.Id, role.Name);
                            }
                            IdentityResult updateWorker = await workerManager.UpdateAsync(worker);
                            if (updateWorker.Succeeded)
                            {
                                return RedirectToAction("Index");
                            }
                            else
                            {
                                ModelState.AddModelError("", "Фигня");
                            }
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "Пользователь не существует");
                    }
                }
                List<SelectListItem> roles = new List<SelectListItem>();
                foreach (var r in RoleManager.Roles)
                {
                    var item = new SelectListItem
                    {
                        Selected = false,
                        Text = r.Name,
                        Value = r.Id
                    };
                    if (editWorker.RoleIds.Contains(r.Id))
                        item.Selected = true;
                    roles.Add(item);
                }
                editWorker.Roles = new SelectList(roles, "Value", "Text");
                return View(editWorker);
            }
            catch (Exception ex)
            {
                ErrorViewModel error = new ErrorViewModel
                {
                    Code = "",
                    Message = ex.Message
                };
                return View("Error", error);
            }
        }

        public ActionResult Delete(string id)
        {
            try
            {
                if (id == null)
                {
                    ErrorViewModel error = new ErrorViewModel
                    {
                        Code = HttpStatusCode.BadRequest.ToString(),
                        Message = "Can not find this page"
                    };
                    return View("Error", error);
                }
                Worker worker = workerManager.FindById(id);
                if (worker == null)
                {
                    ErrorViewModel error = new ErrorViewModel
                    {
                        Code = HttpStatusCode.NotFound.ToString(),
                        Message = "Worker doesn't exist"
                    };
                    return View("Error", error);
                }
                return View(worker);
            }
            catch (Exception ex)
            {
                ErrorViewModel error = new ErrorViewModel
                {
                    Code = "",
                    Message = ex.Message
                };
                return View("Error", error);
            }
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            try
            {
                Worker worker = workerManager.FindById(id);
                workerManager.Delete(worker);
                db.Save();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ErrorViewModel error = new ErrorViewModel
                {
                    Code = "",
                    Message = ex.Message
                };
                return View("Error", error);
            }
        }


        public FileResult Export()
        {
            byte[] result = (db.Workers as WorkerRepository).CsvToMemory();
            return File(result, "text/csv", "Workers.csv");
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        #region ForAjax

        public JsonResult GetOrdered(string field, string orderType)
        {
            var result = (db.Workers as WorkerRepository).Order(field, orderType).Select(s => new
            {
                Id = s.Id,
                Name = s.Name,
                Surname = s.Surname,
                Email = s.Email,
                PhoneNumber = s.PhoneNumber,
                UserName = s.UserName
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetForRequestSearch(string userName)
        {
            IEnumerable<Worker> workers = (db.Workers as WorkerRepository).GetByUserName(userName);
            return Json(workers, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SearchByOption(WorkerSearchOption option)
        {
            var result = (db.Workers as WorkerRepository).Search(option).Select(s => new
            {
                Id = s.Id,
                Name = s.Name,
                Surname = s.Surname,
                Email = s.Email,
                PhoneNumber = s.PhoneNumber,
                UserName = s.UserName
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion

    }
}

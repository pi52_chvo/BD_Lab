﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using curs;
using curs.Models.Room;
using curs.Repositoryes;
using curs.Models.RoomType;
using curs.Models.Worker;
using Microsoft.AspNet.Identity.Owin;
using curs.Models.Request;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace curs.Controllers
{
    public class RoomsController : Controller
    {
        private IUnitOfWork db;

        

        public RoomsController(IUnitOfWork db)
        {
            this.db = db;
        }

        [Authorize]
        public ActionResult Index()
        {
            ViewBag.RoomTypes = db.RoomTypes.Get();
            return View(db.Rooms.Get());
        }

        [Authorize(Roles = "mainadministrator")]
        public ActionResult Create()
        {
            try
            {
                var roomTypes = db.RoomTypes.Get();
                RoomCreateModel roomCreate = new RoomCreateModel
                {
                    RoomTypes = (db.RoomTypes as RoomTypeRepository).GetDropdownList()
                };
                return View(roomCreate);
            }
            catch (Exception ex)
            {
                ErrorViewModel error = new ErrorViewModel
                {
                    Code = "",
                    Message = ex.Message
                };
                return View("Error", error);
            }
        }

        [Authorize(Roles = "mainadministrator")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(RoomCreateModel roomCreate)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (db.Rooms.Get(r => r.Number == roomCreate.Number).FirstOrDefault() != null)
                    {
                        ModelState.AddModelError("", "Комната с данным номером уже есть!");
                        roomCreate.RoomTypes = (db.RoomTypes as RoomTypeRepository).GetDropdownList();
                        return View(roomCreate);
                    }
                    Room room = new Room
                    {
                        Number = roomCreate.Number,
                        RoomType = db.RoomTypes.GetById(roomCreate.RoomTypeId),
                        IsFree = true
                    };
                    db.Rooms.Save(room);
                    db.Save();
                    return RedirectToAction("Index");
                }
                roomCreate.RoomTypes = (db.RoomTypes as RoomTypeRepository).GetDropdownList();
                return View(roomCreate);
            }
            catch (Exception ex)
            {
                ErrorViewModel error = new ErrorViewModel
                {
                    Code = "",
                    Message = ex.Message
                };
                return View("Error", error);
            }
        }

        [Authorize(Roles = "mainadministrator")]
        public ActionResult Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    ErrorViewModel error = new ErrorViewModel
                    {
                        Code = HttpStatusCode.BadRequest.ToString(),
                        Message = "Can not find this page"
                    };
                    return View("Error", error);
                }
                Room room = db.Rooms.GetById((int)id);
                if (room == null)
                {
                    ErrorViewModel error = new ErrorViewModel
                    {
                        Code = HttpStatusCode.NotFound.ToString(),
                        Message = "Room doesn't exist"
                    };
                    return View("Error", error);
                }
                RoomEditModel roomEdit = new RoomEditModel
                {
                    Id = room.Id,
                    Number = room.Number,
                    RoomTypeId = room.RoomType.Id,
                    RoomTypes = (db.RoomTypes as RoomTypeRepository).GetDropdownList(room.RoomType.Id)
                };
                return View(roomEdit);
            }
            catch (Exception ex)
            {
                ErrorViewModel error = new ErrorViewModel
                {
                    Code = "",
                    Message = ex.Message
                };
                return View("Error", error);
            }
        }

        [Authorize(Roles = "mainadministrator")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(RoomEditModel roomEdit)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (db.Rooms.Get(r => r.Number == roomEdit.Number && r.Id != roomEdit.Id).FirstOrDefault() != null)
                    {
                        ModelState.AddModelError("", "Комната с данным номером уже есть!");
                        roomEdit.RoomTypes = (db.RoomTypes as RoomTypeRepository).GetDropdownList();
                        return View(roomEdit);
                    }
                    Room room = db.Rooms.GetById(roomEdit.Id);
                    room.Number = roomEdit.Number;
                    room.RoomType = db.RoomTypes.GetById(roomEdit.RoomTypeId);
                    db.Rooms.Update(room);
                    db.Save();
                    return RedirectToAction("Index");
                }
                roomEdit.RoomTypes = (db.RoomTypes as RoomTypeRepository).GetDropdownList();
                return View(roomEdit);
            }
            catch (Exception ex)
            {
                ErrorViewModel error = new ErrorViewModel
                {
                    Code = "",
                    Message = ex.Message
                };
                return View("Error", error);
            }
        }

        [Authorize(Roles = "mainadministrator")]
        public ActionResult Delete(int? id)
        {
            try
            {
                if (id == null)
                {
                    ErrorViewModel error = new ErrorViewModel
                    {
                        Code = HttpStatusCode.BadRequest.ToString(),
                        Message = "Can not find this page"
                    };
                    return View("Error", error);
                }
                Room room = db.Rooms.GetById((int)id);
                if (room == null)
                {
                    ErrorViewModel error = new ErrorViewModel
                    {
                        Code = HttpStatusCode.NotFound.ToString(),
                        Message = "Room doesn't exist"
                    };
                    return View("Error", error);
                }
                return View(room);
            }
            catch (Exception ex)
            {
                ErrorViewModel error = new ErrorViewModel
                {
                    Code = "",
                    Message = ex.Message
                };
                return View("Error", error);
            }
        }

        [Authorize(Roles = "mainadministrator")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Room room = db.Rooms.GetById(id);
                if(room.IsFree && (db.Requests.Get().Where(r => r.Rooms.Select(rq => rq.Id).Contains(room.Id) && r.ArrivalDate > DateTime.Now).ToList() == null || db.Requests.Get().Where(r => r.Rooms.Select(rq => rq.Id).Contains(room.Id) && r.ArrivalDate > DateTime.Now).ToList().Count == 0))
                {
                    db.Rooms.Delete(room);
                    db.Save();
                    return RedirectToAction("Index");
                }
                ErrorViewModel error = new ErrorViewModel
                {
                    Code = "",
                    Message = "You can not delete this room. This room is not free"
                };
                return View("Error", error);
            }
            catch (Exception ex)
            {
                ErrorViewModel error = new ErrorViewModel
                {
                    Code = "",
                    Message = ex.Message
                };
                return View("Error", error);
            }
        }

        

        public FileResult Export()
        {
            byte[] result = (db.Rooms as RoomRepository).CsvToMemory();
            return File(result, "text/csv", "Rooms.csv");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public JsonResult GetFree(int RoomTypeId, string ArrivalDate, string DepartureDate)
        {
            Regex regex = new Regex(@"\.");
            ArrivalDate = regex.Replace(ArrivalDate, "");
            DepartureDate = regex.Replace(DepartureDate, "");
            SqlParameter roomTypeId = new SqlParameter("@roomTypeId", RoomTypeId);
            SqlParameter arrivalDate = new SqlParameter("@arrivalDate", ArrivalDate);
            SqlParameter departureDate = new SqlParameter("@departureDate", DepartureDate);
            var result = db.Database.SqlQuery<JsonResultRooms>("sp_get_free_room_by_period @roomTypeId, @arrivalDate, @departureDate", roomTypeId, arrivalDate, departureDate);
            return Json(result.ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetOrdered(string field, string orderType)
        {
            var result = (db.Rooms as RoomRepository).Order(field, orderType).Select(s => new
            {
                Id = s.Id,
                Number = s.Number,
                IsFree = s.IsFree,
                Type = s.RoomType.Title,
                Price = s.RoomType.Price
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SearchByOption(RoomSearchOption option)
        {
            var result = (db.Rooms as RoomRepository).Search(option).Select(s => new
            {
                Id = s.Id,
                Number = s.Number,
                IsFree = s.IsFree,
                Type = s.RoomType.Title,
                Price = s.RoomType.Price
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /*public JsonResult SearchByOption(ServiceSearchOption option)
        {
            var result = (db.Services as ServiceRepository).Search(option).Select(s => new { Id = s.Id, Title = s.Title, Price = s.Price });
            return Json(result, JsonRequestBehavior.AllowGet);
        }*/
    }

    public class JsonResultRooms
    {
        public int id { get; set; }

        public string number { get; set; }
    }
}

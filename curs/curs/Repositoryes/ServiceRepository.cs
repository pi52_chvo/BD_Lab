﻿using curs.Models.Service;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CsvHelper;
using System.IO;

namespace curs.Repositoryes
{
    public class ServiceRepository : IBasicRepository<Service>
    {
        private HotelContext db;

        public ServiceRepository(HotelContext db)
        {
            this.db = db;
        }

        public void Delete(Service entity)
        {
            db.Services.Remove(entity);
        }

        public IEnumerable<Service> Get()
        {
            return db.Services.ToList();
        }

        public IEnumerable<Service> Get(Func<Service, bool> exprecion)
        {
            return db.Services.Where(exprecion).ToList();
        }

        public Service GetById(int id)
        {
            return db.Services.Find(id);
        }

        public void Save(Service entity)
        {
            db.Services.Add(entity);
        }

        public void Update(Service entity)
        {
            db.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }

        public IEnumerable<Service> Search(ServiceSearchOption option)
        {
            IQueryable<Service> query = db.Services;
            if(option.Title != null)
            {
                query = query.Where(s => s.Title.ToLower().Contains(option.Title.ToLower()));
            }
            if (!string.IsNullOrEmpty(option.Price))
            {
                string[] arr = option.Price.Split(' ');
                double Price = double.Parse(arr[1]);
                if (arr[0] == "=")
                {
                    query = query.Where(s => s.Price == Price);
                }
                else if (arr[0] == ">")
                {
                    query = query.Where(s => s.Price > Price);
                }
                else
                {
                    query = query.Where(s => s.Price < Price);
                }
            }
            return query.ToList();
        }

        public SelectList GetDropdownList()
        {
            var servicesSelectList = new List<SelectListItem>();
            foreach (Service service in db.Services.ToList())
            {
                var item = new SelectListItem
                {
                    Selected = false,
                    Text = service.Title,
                    Value = service.Id.ToString()
                };
                servicesSelectList.Add(item);
            }
            return new SelectList(servicesSelectList, "Value", "Text");
        }

        public SelectList GetDropdownList(int[] ids)
        {
            var servicesSelectList = new List<SelectListItem>();
            foreach (Service service in db.Services.ToList())
            {
                var item = new SelectListItem
                {
                    Selected = false,
                    Text = service.Title,
                    Value = service.Id.ToString()
                };
                if (ids.Contains(service.Id))
                {
                    item.Selected = true;
                }
                servicesSelectList.Add(item);
            }
            return new SelectList(servicesSelectList, "Value", "Text");
        }

        public IEnumerable<Service> Order(string field, string type)
        {
            IQueryable<Service> query = db.Services;
            if(field == "Title")
            {
                if(type == "asc")
                {
                    query = query.OrderBy(s => s.Title);
                }
                else
                {
                    query = query.OrderByDescending(s => s.Title);
                }
                
            }
            else if (field == "Price")
            {
                if (type == "asc")
                {
                    query = query.OrderBy(s => s.Price);
                }
                else
                {
                    query = query.OrderByDescending(s => s.Price);
                }

            }
            return query.ToList();
        }

        public byte[] CsvToMemory()
        {
            using (var memoryStream = new MemoryStream())
            {
                using (var streamWriter = new StreamWriter(memoryStream))
                {
                    using (var csv = new CsvWriter(streamWriter))
                    {
                        csv.WriteRecords(db.Services.ToList());
                        streamWriter.Flush();
                        return memoryStream.ToArray();
                    }
                }
            }
        }
    }
}
﻿using CsvHelper;
using curs.Models.RoomType;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace curs.Repositoryes
{
    public class RoomTypeRepository : IBasicRepository<RoomType>
    {
        private HotelContext db;

        public RoomTypeRepository(HotelContext db)
        {
            this.db = db;
        }

        public void Delete(RoomType entity)
        {
            db.RoomTypes.Remove(entity);
        }

        public IEnumerable<RoomType> Get()
        {
            return db.RoomTypes.ToList();
        }

        public IEnumerable<RoomType> Get(Func<RoomType, bool> exprecion)
        {
            return db.RoomTypes.Where(exprecion).ToList();
        }

        public RoomType GetById(int id)
        {
            return db.RoomTypes.Find(id);
        }

        public void Save(RoomType entity)
        {
            db.RoomTypes.Add(entity);
        }

        public void Update(RoomType entity)
        {
            db.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }

        public IEnumerable<RoomType> Search(RoomTypeSearchOption option)
        {
            IQueryable<RoomType> query = db.RoomTypes;
            if (!string.IsNullOrEmpty(option.Title))
            {
                query = query.Where(s => s.Title.ToLower().Contains(option.Title.ToLower()));
            }
            if (!string.IsNullOrEmpty(option.Price))
            {
                string[] arr = option.Price.Split(' ');
                double Price = double.Parse(arr[1]);
                if(arr[0] == "=")
                {
                    query = query.Where(s => s.Price == Price);
                }
                else if(arr[0] == ">")
                {
                    query = query.Where(s => s.Price > Price);
                }
                else
                {
                    query = query.Where(s => s.Price < Price);
                }        
            }
            return query.ToList();
        }

        public SelectList GetDropdownList(Func<RoomType, bool> exprecion)
        {
            var roomTypesSelectList = new List<SelectListItem>();
            foreach (RoomType roomType in db.RoomTypes.Where(exprecion))
            {
                var item = new SelectListItem
                {
                    Selected = false,
                    Text = roomType.Title,
                    Value = roomType.Id.ToString()
                };
                roomTypesSelectList.Add(item);
            }
            return new SelectList(roomTypesSelectList, "Value", "Text");
        }
        public SelectList GetDropdownList()
        {
            var roomTypesSelectList = new List<SelectListItem>();
            foreach (RoomType roomType in db.RoomTypes.ToList())
            {
                var item = new SelectListItem
                {
                    Selected = false,
                    Text = roomType.Title,
                    Value = roomType.Id.ToString()
                };
                roomTypesSelectList.Add(item);
            }
            return new SelectList(roomTypesSelectList, "Value", "Text");
        }
        public SelectList GetDropdownList(int id)
        {
            var roomTypesSelectList = new List<SelectListItem>();
            foreach (RoomType roomType in db.RoomTypes.ToList())
            {
                var item = new SelectListItem
                {
                    Selected = false,
                    Text = roomType.Title,
                    Value = roomType.Id.ToString()
                };
                if(roomType.Id == id)
                {
                    item.Selected = true;
                }
                roomTypesSelectList.Add(item);
            }
            return new SelectList(roomTypesSelectList, "Value", "Text");
        }

        public IEnumerable<RoomType> Order(string field, string type)
        {
            IQueryable<RoomType> query = db.RoomTypes;
            if (field == "Title")
            {
                if (type == "asc")
                {
                    query = query.OrderBy(s => s.Title);
                }
                else
                {
                    query = query.OrderByDescending(s => s.Title);
                }

            }
            else if (field == "Price")
            {
                if (type == "asc")
                {
                    query = query.OrderBy(s => s.Price);
                }
                else
                {
                    query = query.OrderByDescending(s => s.Price);
                }

            }
            return query.ToList();
        }

            public byte[] CsvToMemory()
        {
            using (var memoryStream = new MemoryStream())
            {
                using (var streamWriter = new StreamWriter(memoryStream))
                {
                    using (var csv = new CsvWriter(streamWriter))
                    {
                        csv.WriteRecords(db.RoomTypes.ToList());
                        streamWriter.Flush();
                        return memoryStream.ToArray();
                    }
                }
            }
        }
    }
}
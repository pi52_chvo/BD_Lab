﻿using CsvHelper;
using curs.Models;
using curs.Models.Room;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace curs.Repositoryes
{
    public class RoomRepository : IBasicRepository<Room>
    {
        private HotelContext db;

        public RoomRepository(HotelContext db)
        {
            this.db = db;
        }

        public void Delete(Room entity)
        {
            db.Rooms.Remove(entity);
        }

        public IEnumerable<Room> Get()
        {
            return db.Rooms.ToList();
        }

        public IEnumerable<Room> Get(Func<Room, bool> exprecion)
        {
            return db.Rooms.Where(exprecion).ToList();
        }

        public Room GetById(int id)
        {
            return db.Rooms.Find(id);
        }

        public void Save(Room entity)
        {
            db.Rooms.Add(entity);
        }

        public void Update(Room entity)
        {
            db.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }

        public SelectList GetDropdownList()
        {
            var roomsSelectList = new List<SelectListItem>();
            foreach (Room roomType in db.Rooms.ToList())
            {
                var item = new SelectListItem
                {
                    Selected = false,
                    Text = roomType.Number,
                    Value = roomType.Id.ToString()
                };
                roomsSelectList.Add(item);
            }
            return new SelectList(roomsSelectList, "Value", "Text");
        }

        public SelectList GetDropdownList(int[] ids)
        {
            var roomsSelectList = new List<SelectListItem>();
            foreach (Room roomType in db.Rooms.ToList())
            {
                var item = new SelectListItem
                {
                    Selected = false,
                    Text = roomType.Number,
                    Value = roomType.Id.ToString()
                };
                if(ids.Contains(roomType.Id))
                {
                    item.Selected = true;
                }
                roomsSelectList.Add(item);
            }
            return new SelectList(roomsSelectList, "Value", "Text");
        }

        public IEnumerable<Room> Search(RoomSearchOption option)
        {
            IQueryable<Room> query = db.Rooms;
            if (!string.IsNullOrEmpty(option.Number))
            {
                var room = query.Where(r => r.Number == option.Number).ToList();
                if (room.Count > 0) return room;
                query = query.Where(s => s.Number.ToLower().Contains(option.Number.ToLower()));
            }
            if (!string.IsNullOrEmpty(option.RoomTypeIds))
            {
                int[] ids = option.RoomTypeIds.Split(' ').Select(i => int.Parse(i)).ToArray();
                query = query.Where(r => ids.Contains(r.RoomType.Id));
            }
            if(option.IsFree != null)
            {
                query = query.Where(r => r.IsFree == option.IsFree);
            }
            if (!string.IsNullOrEmpty(option.Price))
            {
                string[] arr = option.Price.Split(' ');
                double Price = double.Parse(arr[1]);
                if (arr[0] == "=")
                {
                    query = query.Where(s => s.RoomType.Price == Price);
                }
                else if (arr[0] == ">")
                {
                    query = query.Where(s => s.RoomType.Price > Price);
                }
                else
                {
                    query = query.Where(s => s.RoomType.Price < Price);
                }
            }
            return query.ToList();
        }

        public IEnumerable<Room> Order(string field, string type)
        {
            IQueryable<Room> query = db.Rooms;
            if (field == "Number")
            {
                if (type == "asc")
                {
                    query = query.OrderBy(s => s.Number);
                }
                else
                {
                    query = query.OrderByDescending(s => s.Number);
                }
            }
            else if (field == "Price")
            {
                if (type == "asc")
                {
                    query = query.OrderBy(s => s.RoomType.Price);
                }
                else
                {
                    query = query.OrderByDescending(s => s.RoomType.Price);
                }
            }
            else if (field == "IsFree")
            {
                if (type == "asc")
                {
                    query = query.OrderBy(s => s.IsFree);
                }
                else
                {
                    query = query.OrderByDescending(s => s.IsFree);
                }
            }
            else if (field == "RoomType")
            {
                if (type == "asc")
                {
                    query = query.OrderBy(s => s.RoomType.Title);
                }
                else
                {
                    query = query.OrderByDescending(s => s.RoomType.Title);
                }
            }
            return query.ToList();
        }

        public byte[] CsvToMemory()
        {
            using (var memoryStream = new MemoryStream())
            {
                using (var streamWriter = new StreamWriter(memoryStream))
                {
                    using (var csv = new CsvWriter(streamWriter))
                    {
                        csv.WriteRecords(db.Rooms.ToList());
                        streamWriter.Flush();
                        return memoryStream.ToArray();
                    }
                }
            }
        }
    }
}
﻿using CsvHelper;
using curs.Models.Service;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace curs.Repositoryes
{
    public class OrderedServiceRepository : IBasicRepository<OrderedService>
    {
        private HotelContext db;

        public OrderedServiceRepository(HotelContext db)
        {
            this.db = db;
        }

        public void Delete(OrderedService entity)
        {
            db.OrderedServices.Remove(entity);
        }

        public IEnumerable<OrderedService> Get()
        {
            return db.OrderedServices.ToList();
        }

        public IEnumerable<OrderedService> Get(Func<OrderedService, bool> exprecion)
        {
            return db.OrderedServices.Where(exprecion).ToList();
        }

        public OrderedService GetById(int id)
        {
            return db.OrderedServices.Find(id);
        }

        public void Save(OrderedService entity)
        {
            db.OrderedServices.Add(entity);
        }

        public void Update(OrderedService entity)
        {
            db.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }

        public byte[] CsvToMemory()
        {
            using (var memoryStream = new MemoryStream())
            {
                using (var streamWriter = new StreamWriter(memoryStream))
                {
                    using (var csv = new CsvWriter(streamWriter))
                    {
                        csv.WriteRecords(db.OrderedServices.ToList());
                        streamWriter.Flush();
                        return memoryStream.ToArray();
                    }
                }
            }
        }
    }
}
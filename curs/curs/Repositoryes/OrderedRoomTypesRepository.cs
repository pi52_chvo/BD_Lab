﻿using CsvHelper;
using curs.Models.RoomType;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace curs.Repositoryes
{
    public class OrderedRoomTypesRepository : IBasicRepository<OrderedRoomType>
    {
        private HotelContext db;

        public OrderedRoomTypesRepository(HotelContext db)
        {
            this.db = db;
        }

        public void Delete(OrderedRoomType entity)
        {
            db.OrderedRoomTypes.Remove(entity);
        }

        public IEnumerable<OrderedRoomType> Get()
        {
            return db.OrderedRoomTypes.ToList();
        }

        public IEnumerable<OrderedRoomType> Get(Func<OrderedRoomType, bool> exprecion)
        {
            return db.OrderedRoomTypes.Where(exprecion).ToList();
        }

        public OrderedRoomType GetById(int id)
        {
            return db.OrderedRoomTypes.Find(id);
        }

        public void Save(OrderedRoomType entity)
        {
            db.OrderedRoomTypes.Add(entity);
        }

        public void Update(OrderedRoomType entity)
        {
            db.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }

        public byte[] CsvToMemory()
        {
            using (var memoryStream = new MemoryStream())
            {
                using (var streamWriter = new StreamWriter(memoryStream))
                {
                    using (var csv = new CsvWriter(streamWriter))
                    {
                        csv.WriteRecords(db.OrderedRoomTypes.ToList());
                        streamWriter.Flush();
                        return memoryStream.ToArray();
                    }
                }
            }
        }
    }
}
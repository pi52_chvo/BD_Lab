﻿using CsvHelper;
using curs.Models.Worker;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace curs.Repositoryes
{
    public class WorkerRepository : IBasicRepository<Worker>
    {
        private HotelContext db;

        public WorkerRepository(HotelContext db)
        {
            this.db = db;
        }

        public void Delete(Worker entity)
        {
            db.Users.Remove(entity);
        }

        public IEnumerable<Worker> Get()
        {
            return db.Users.ToList();
        }

        public IEnumerable<Worker> Get(Func<Worker, bool> exprecion)
        {
            return db.Users.Where(exprecion).ToList();
        }

        public Worker GetById(int id)
        {
            return db.Users.Find(id);
        }

        public void Save(Worker entity)
        {
            db.Users.Add(entity);
        }

        public void Update(Worker entity)
        {
            db.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }

        public IEnumerable<Worker> Search(WorkerSearchOption option)
        {
            IQueryable<Worker> query = db.Users;
            if (!string.IsNullOrEmpty(option.Name))
            {
                query = query.Where(s => s.Name.ToLower().Contains(option.Name.ToLower()));
            }
            if (!string.IsNullOrEmpty(option.Surname))
            {
                query = query.Where(s => s.Surname.ToLower().Contains(option.Surname.ToLower()));
            }
            if (!string.IsNullOrEmpty(option.Email))
            {
                query = query.Where(s => s.Email.ToLower().Contains(option.Email.ToLower()));
            }
            if (!string.IsNullOrEmpty(option.PhoneNumber))
            {
                query = query.Where(s => s.PhoneNumber.ToLower().Contains(option.PhoneNumber.ToLower()));
            }
            return query.ToList();
        }

        public IEnumerable<Worker> GetByUserName(string userName)
        {
            IQueryable<Worker> workers = db.Users.Where(u => u.UserName.StartsWith(userName));
            return workers.ToList();
        }

        public IEnumerable<Worker> Order(string field, string type)
        {
            IQueryable<Worker> query = db.Users;
            if (field == "Name")
            {
                if (type == "asc")
                {
                    query = query.OrderBy(s => s.Name);
                }
                else
                {
                    query = query.OrderByDescending(s => s.Name);
                }
            }
            else if (field == "Surname")
            {
                if (type == "asc")
                {
                    query = query.OrderBy(s => s.Surname);
                }
                else
                {
                    query = query.OrderByDescending(s => s.Surname);
                }
            }
            else if (field == "Email")
            {
                if (type == "asc")
                {
                    query = query.OrderBy(s => s.Email);
                }
                else
                {
                    query = query.OrderByDescending(s => s.Email);
                }
            }
            else if (field == "PhoneNumber")
            {
                if (type == "asc")
                {
                    query = query.OrderBy(s => s.PhoneNumber);
                }
                else
                {
                    query = query.OrderByDescending(s => s.PhoneNumber);
                }
            }
            else if (field == "UserName")
            {
                if (type == "asc")
                {
                    query = query.OrderBy(s => s.UserName);
                }
                else
                {
                    query = query.OrderByDescending(s => s.UserName);
                }
            }
            return query.ToList();
        }

        public byte[] CsvToMemory()
        {
            using (var memoryStream = new MemoryStream())
            {
                using (var streamWriter = new StreamWriter(memoryStream))
                {
                    using (var csv = new CsvWriter(streamWriter))
                    {
                        csv.WriteRecords(db.Users.ToList());
                        streamWriter.Flush();
                        return memoryStream.ToArray();
                    }
                }
            }
        }
    }
}
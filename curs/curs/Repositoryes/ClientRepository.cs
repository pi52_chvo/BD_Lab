﻿using CsvHelper;
using curs.Models.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace curs.Repositoryes
{
    public class ClientRepository : IBasicRepository<Client>
    {
        private HotelContext db;

        public ClientRepository(HotelContext db)
        {
            this.db = db;
        }

        public void Delete(Client entity)
        {
            db.Clients.Remove(entity);
        }

        public IEnumerable<Client> Get()
        {
            return db.Clients.ToList();
        }

        public IEnumerable<Client> Get(Func<Client, bool> exprecion)
        {
            return db.Clients.Where(exprecion).ToList();
        }

        public Client GetById(int id)
        {
            return db.Clients.Find(id);
        }

        public void Save(Client entity)
        {
            db.Clients.Add(entity);
        }

        public void Update(Client entity)
        {
            db.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }

        public IEnumerable<Client> Search(ClientSearchOption option)
        {
            IQueryable<Client> query = db.Clients;
            if (!string.IsNullOrEmpty(option.Name))
            {
                query = query.Where(s => s.Name.ToLower().Contains(option.Name.ToLower()));
            }
            if (!string.IsNullOrEmpty(option.Surname))
            {
                query = query.Where(s => s.Surname.ToLower().Contains(option.Surname.ToLower()));
            }
            if (!string.IsNullOrEmpty(option.Email))
            {
                query = query.Where(s => s.Email.ToLower().Contains(option.Email.ToLower()));
            }
            if (!string.IsNullOrEmpty(option.PhoneNumber))
            {
                query = query.Where(s => s.Phonenumber.ToLower().Contains(option.PhoneNumber.ToLower()));
            }
            if (!string.IsNullOrEmpty(option.Country))
            {
                query = query.Where(s => s.Country.ToLower().Contains(option.Country.ToLower()));
            }
            if (!string.IsNullOrEmpty(option.Address))
            {
                query = query.Where(s => s.Address.ToLower().Contains(option.Address.ToLower()));
            }
            if (!string.IsNullOrEmpty(option.Locality))
            {
                query = query.Where(s => s.Locality.ToLower().Contains(option.Locality.ToLower()));
            }
            if (!string.IsNullOrEmpty(option.Index))
            {
                query = query.Where(s => s.Index.ToLower().Contains(option.Index.ToLower()));
            }
            return query.ToList();
        }

        public IEnumerable<Client> Order(string field, string type)
        {
            IQueryable<Client> query = db.Clients;
            if (field == "Name")
            {
                if (type == "asc")
                {
                    query = query.OrderBy(s => s.Name);
                }
                else
                {
                    query = query.OrderByDescending(s => s.Name);
                }
            }
            else if (field == "Surname")
            {
                if (type == "asc")
                {
                    query = query.OrderBy(s => s.Surname);
                }
                else
                {
                    query = query.OrderByDescending(s => s.Surname);
                }
            }
            else if (field == "Email")
            {
                if (type == "asc")
                {
                    query = query.OrderBy(s => s.Email);
                }
                else
                {
                    query = query.OrderByDescending(s => s.Email);
                }
            }
            else if (field == "Phonenumber")
            {
                if (type == "asc")
                {
                    query = query.OrderBy(s => s.Phonenumber);
                }
                else
                {
                    query = query.OrderByDescending(s => s.Phonenumber);
                }
            }
            else if (field == "Country")
            {
                if (type == "asc")
                {
                    query = query.OrderBy(s => s.Country);
                }
                else
                {
                    query = query.OrderByDescending(s => s.Country);
                }
            }
            return query.ToList();
        }

        public byte[] CsvToMemory()
        {
            using (var memoryStream = new MemoryStream())
            {
                using (var streamWriter = new StreamWriter(memoryStream))
                {
                    using (var csv = new CsvWriter(streamWriter))
                    {
                        csv.WriteRecords(db.Clients.ToList());
                        streamWriter.Flush();
                        return memoryStream.ToArray();
                    }
                }
            }
        }
    }
}
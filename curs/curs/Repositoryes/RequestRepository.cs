﻿using CsvHelper;
using curs.Models.Request;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Data.Entity;
using System.Web;
using curs.Controllers;
using curs.Models.RoomType;
using curs.Models.Service;

namespace curs.Repositoryes
{
    public class RequestRepository : IBasicRepository<Request>
    {
        private HotelContext db;

        public RequestRepository(HotelContext db)
        {
            this.db = db;
        }

        public void Delete(Request entity)
        {
            db.Requests.Remove(entity);
        }

        public IEnumerable<Request> Get()
        {
            return db.Requests.ToList();
        }

        public IEnumerable<Request> Get(Func<Request, bool> exprecion)
        {
            return db.Requests.Where(exprecion).ToList();
        }

        public Request GetById(int id)
        {
            return db.Requests.Find(id);
        }

        public void Save(Request entity)
        {
            db.Requests.Add(entity);
        }

        public void Update(Request entity)
        {
            db.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }

        public IEnumerable<Request> Search(RequestSearchOption option)
        {
            IQueryable<Request> query = db.Requests;
            IEnumerable<Request> result = null;
            if (!string.IsNullOrEmpty(option.ArrivalDate))
            {
                string[] dateTime = option.ArrivalDate.Split(' ');
                if(dateTime.Length == 1)
                {
                    string[] dateArray = option.ArrivalDate.Split('.');
                    if (dateArray.Length == 1)
                    {
                        int year = int.Parse(dateArray[0]);
                        query = query.Where(r => r.ArrivalDate.Year == year);
                    }
                    else if (dateArray.Length == 2)
                    {
                        int year = int.Parse(dateArray[0]);
                        int month = int.Parse(dateArray[1]);
                        query = query.Where(r => r.ArrivalDate.Year == year && r.ArrivalDate.Month == month);
                    }
                    else if (dateArray.Length == 3)
                    {
                        int year = int.Parse(dateArray[0]);
                        int month = int.Parse(dateArray[1]);
                        int day = int.Parse(dateArray[2]);
                        query = query.Where(r => r.ArrivalDate.Year == year && r.ArrivalDate.Month == month && r.ArrivalDate.Day == day);
                    }
                }
                else
                {
                    var arrival = GetDateFromString(option.ArrivalDate);
                    query = query.Where(r => r.ArrivalDate.CompareTo(arrival) == 0);
                }
            }
            if (!string.IsNullOrEmpty(option.DepartureDate))
            {
                string[] dateTime = option.DepartureDate.Split(' ');
                if (dateTime.Length == 1)
                {
                    string[] dateArray = option.DepartureDate.Split('.');
                    if (dateArray.Length == 1)
                    {
                        int year = int.Parse(dateArray[0]);
                        query = query.Where(r => r.DepartureDate.Year == year);
                    }
                    else if (dateArray.Length == 2)
                    {
                        int year = int.Parse(dateArray[0]);
                        int month = int.Parse(dateArray[1]);
                        query = query.Where(r => r.DepartureDate.Year == year && r.DepartureDate.Month == month);
                    }
                    else if (dateArray.Length == 3)
                    {
                        int year = int.Parse(dateArray[0]);
                        int month = int.Parse(dateArray[1]);
                        int day = int.Parse(dateArray[2]);
                        query = query.Where(r => r.DepartureDate.Year == year && r.DepartureDate.Month == month && r.DepartureDate.Day == day);
                    }
                }
                else
                {
                    var arrival = GetDateFromString(option.DepartureDate);
                    query = query.Where(r => r.DepartureDate.CompareTo(arrival) == 0);
                }
            }
            if (!string.IsNullOrEmpty(option.ReservationDate))
            {
                string[] dateTime = option.ReservationDate.Split(' ');
                if (dateTime.Length == 1)
                {
                    string[] dateArray = option.ReservationDate.Split('.');
                    if (dateArray.Length == 1)
                    {
                        int year = int.Parse(dateArray[0]);
                        query = query.Where(r => r.ReservationDate.Year == year);
                    }
                    else if (dateArray.Length == 2)
                    {
                        int year = int.Parse(dateArray[0]);
                        int month = int.Parse(dateArray[1]);
                        query = query.Where(r => r.ReservationDate.Year == year && r.ReservationDate.Month == month);
                    }
                    else if (dateArray.Length == 3)
                    {
                        int year = int.Parse(dateArray[0]);
                        int month = int.Parse(dateArray[1]);
                        int day = int.Parse(dateArray[2]);
                        query = query.Where(r => r.ReservationDate.Year == year && r.ReservationDate.Month == month && r.ReservationDate.Day == day);
                    }
                }
                else
                {
                    var arrival = GetDateFromString(option.ReservationDate);
                    query = query.Where(r => r.ReservationDate.CompareTo(arrival) == 0);
                }
            }
            if(option.Adult != null)
            {
                query = query.Where(r => r.AdultCount == option.Adult);
            }
            if (option.Child != null)
            {
                query = query.Where(r => r.ChildCount == option.Child);
            }
            if(!string.IsNullOrEmpty(option.ClientEmail))
            {
                query = query.Where(r => r.Client.Email.StartsWith(option.ClientEmail));
            }
            if (!string.IsNullOrEmpty(option.WorkerEmail))
            {
                query = query.Where(r => r.Worker.Email.StartsWith(option.WorkerEmail));
            }
            if (!string.IsNullOrEmpty(option.Price))
            {
                string[] arr = option.Price.Split(' ');
                double price = double.Parse(arr[1]);
                if (arr[0] == "=")
                {
                    result = query.ToList().Where(s => ((s.DepartureDate.Day - s.ArrivalDate.Day) * (s.OrderedRoomTypes.Sum(rt => rt.Price) + (s.Services.Sum(rt => rt.Price ?? 0)))) == price);
                }
                else if (arr[0] == ">")
                {
                    result = query.ToList().Where(s => ((s.DepartureDate.Day - s.ArrivalDate.Day) * (s.OrderedRoomTypes.Sum(rt => rt.Price) + (s.Services.Sum(rt => rt.Price ?? 0)))) > price);
                }
                else
                {
                    result = query.ToList().Where(s => ((s.DepartureDate.Day - s.ArrivalDate.Day) * (s.OrderedRoomTypes.Sum(rt => rt.Price) + (s.Services.Sum(rt => rt.Price ?? 0)))) < price);
                }
            }
            return result ?? query.ToList();
        }

        private DateTime GetDateFromString(string dateString)
        {
            string[] str = dateString.Split(' ');
            string[] date = str[0].Split('.');
            string[] time = str[1].Split(':');
            DateTime dateResult = new DateTime(int.Parse(date[0]), int.Parse(date[1]), int.Parse(date[2]), int.Parse(time[0]), int.Parse(time[1]), int.Parse(time[2]));
            return dateResult;
        }

        public IEnumerable<Request> Order(string field, string type)
        {
            IQueryable<Request> query = db.Requests;
            IEnumerable<Request> result = null;
            if (field == "ArrivalDate")
            {
                if (type == "asc")
                {
                    query = query.OrderBy(s => s.ArrivalDate);
                }
                else
                {
                    query = query.OrderByDescending(s => s.ArrivalDate);
                }
            }
            else if (field == "DepartureDate")
            {
                if (type == "asc")
                {
                    query = query.OrderBy(s => s.DepartureDate);
                }
                else
                {
                    query = query.OrderByDescending(s => s.DepartureDate);
                }
            }
            else if (field == "ReservationDate")
            {
                if (type == "asc")
                {
                    query = query.OrderBy(s => s.ReservationDate);
                }
                else
                {
                    query = query.OrderByDescending(s => s.ReservationDate);
                }
            }
            else if (field == "ClientSurname")
            {
                if (type == "asc")
                {
                    query = query.OrderBy(s => s.Client.Surname);
                }
                else
                {
                    query = query.OrderByDescending(s => s.Client.Surname);
                }
            }
            else if (field == "WorkerSurname")
            {
                if (type == "asc")
                {
                    query = query.OrderBy(s => s.Worker.Surname);
                }
                else
                {
                    query = query.OrderByDescending(s => s.Worker.Surname);
                }
            }
            else if (field == "TotalPrice")
            {
                if (type == "asc")
                {
                    result = query.ToList().OrderBy(s => ((s.DepartureDate - s.ArrivalDate).TotalDays * (s.OrderedRoomTypes.Sum(rt => rt.Price) + (s.Services.Sum(rt => rt.Price ?? 0)))));
                }
                else
                {
                    result = query.ToList().OrderByDescending(s => ((s.DepartureDate - s.ArrivalDate).TotalDays * (s.OrderedRoomTypes.Sum(rt => rt.Price) + s.Services.Sum(rt => rt.Price ?? 0))));
                }
            }

            return result ?? query.ToList();
        }

        public StatisticViewModel Statistics (int dateType, string date)
        {
            IQueryable<Request> result = db.Requests;
            string[] datePart = date.Split(' ')[0].Split('.');
            int year = int.Parse(datePart[2]);
            int month = int.Parse(datePart[1]);
            int day = int.Parse(datePart[0]);
            if (dateType == 0)
            {
                result = result.Where(r => r.ReservationDate.Year == year && r.ReservationDate.Month == month && r.ReservationDate.Day == day);
            }
            else if(dateType == 1)
            {
                result = result.Where(r => r.ReservationDate.Year == year && r.ReservationDate.Month == month);
            }
            else if (dateType == 2)
            {
                result = result.Where(r => r.ReservationDate.Year == year);
            }
            var requests = result.ToList();
            var roomTypes = requests.Select(r => r.OrderedRoomTypes);
            List<OrderedRoomType> orderedRoomType = new List<OrderedRoomType>();
            foreach(var item in roomTypes)
            {
                foreach(var roomType in item)
                {
                    orderedRoomType.Add(roomType);
                }
            }
            var services = requests.Select(s => s.Services);
            List<OrderedService> orderedServices = new List<OrderedService>();
            foreach(var item in services)
            {
                foreach(var service in item)
                {
                    orderedServices.Add(service);
                }
            }
            StatisticViewModel statisticResult = new StatisticViewModel
            {
                Clients = requests.Count(),
                Spent = requests.Select(s => ((s.DepartureDate.Day - s.ArrivalDate.Day) * (s.OrderedRoomTypes.Sum(rt => rt.Price) + (s.Services.Sum(rt => rt.Price ?? 0))))).Sum(),
                RoomTypes = orderedRoomType.GroupBy(r => r.Title).Select(g => new RoomTypeStatistic { Type = g.Key, Count = g.Count() }).ToList(),
                AllRoomTypes = db.RoomTypes.Select(rt => new RoomTypeStatistic { Type = rt.Title, Count = 0 }).ToList(),
                Services = orderedServices.GroupBy(os => os.Title).Select(os => new ServiceStatistic { Title = os.Key, Count = os.Count() }).ToList(),
                AllServices = db.Services.Select(s => new ServiceStatistic { Title = s.Title, Count = 0 }).ToList()
            };
            return statisticResult;
        }

        public byte[] CsvToMemory()
        {
            using (var memoryStream = new MemoryStream())
            {
                using (var streamWriter = new StreamWriter(memoryStream))
                {
                    using (var csv = new CsvWriter(streamWriter))
                    {
                        csv.WriteRecords(db.Requests.ToList().Select(r => new
                        {
                            Id = r.Id,
                            Arrival = r.ArrivalDate,
                            Departure = r.DepartureDate,
                            Reservation = r.ReservationDate,
                            Adult = r.AdultCount,
                            Child = r.ChildCount ?? 0,
                            TotalPrice = ((r.DepartureDate - r.ArrivalDate).TotalDays * (r.OrderedRoomTypes.Sum(rt => rt.Price) + (r.Services.Sum(rt => rt.Price ?? 0)))),
                            ClientName = r.Client.Name,
                            ClientSurname = r.Client.Surname,
                            ClientEmail = r.Client.Email,
                            ClientPhone = r.Client.Phonenumber,
                            WorkerId = r.Worker.Id,
                            WorkerEmail = r.Worker.Email,
                            WorkerPhone = r.Worker.PhoneNumber
                        }).ToList());
                        streamWriter.Flush();
                        return memoryStream.ToArray();
                    }
                }
            }
        }
    }
}
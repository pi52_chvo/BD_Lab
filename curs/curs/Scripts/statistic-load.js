﻿$(document).ready(function() {
    $("form").on("submit", function(event) {
        $("#statistic-result .card > h1").addClass("hidden");
        $("#statistic-result .card > .loading-img").removeClass("hidden");
        $("#statistic-result .card > .card-body").addClass("hidden");
        event.preventDefault();
        var date = $("input[name='Date']").val();
        var dateType = $("select[name='DateType'] option:selected").val();
        $.ajax({
            url: "/Requests/GetStatistic",
            method: "GET",
            data: {"dateType": dateType, "date": date},
            success: function(data) {
                console.log(data);
                $("#total-spent-td").html(data.Spent + " $");
                $("#total-clients-td").html(data.Clients);
                
                var roomTypeLabels = [];
                var roomTypeCount = [];

                for(var i = 0; i < data.AllRoomTypes.length; i++) {
                    roomTypeLabels.push(data.AllRoomTypes[i].Type);
                    roomTypeCount.push(data.AllRoomTypes[i].Count);
                }
                for(var i = 0; i < data.RoomTypes.length; i++) {
                    var index = roomTypeLabels.findIndex(function(elem, index, arr) {
                        if(elem == data.RoomTypes[i].Type) {
                            return true;
                        }
                        else {
                            return false;
                        }
                    });
                    if(index == -1) {
                        roomTypeLabels.push(data.RoomTypes[i].Type);
                        roomTypeCount.push(data.RoomTypes[i].Count);
                    }
                    else {
                        roomTypeCount[i] = data.RoomTypes[i].Count;
                    }
                }

                var canvas = $("#roomTypeChart");
        var chartOptions = {
            legend: {
                display: true,
                position: 'top',
                labels: {
                    boxWidth: 40,
                    fontColor: 'black'
                }
            }
        };

        var incomeData = {
            labels: roomTypeLabels,
            datasets: [{
                label: "Ordering count",
                data: roomTypeCount,
                borderColor: "#3cba9f",
                fill: true,
                backgroundColor: '#8bc6c2',
                pointRadius: 5
            }]
        };

        var lineChart = new Chart(canvas, {
            type: 'bar',
            data: incomeData,
            options: chartOptions
        });

//-----------------------------------------------------
                var serviceLabels = [];
                var serviceCount = [];

                for(var i = 0; i < data.AllServices.length; i++) {
                    serviceLabels.push(data.AllServices[i].Title);
                    serviceCount.push(data.AllServices[i].Count);
                }
                for(var i = 0; i < data.Services.length; i++) {
                    var index = serviceLabels.findIndex(function(elem, index, arr) {
                        if(elem == data.Services[i].Title) {
                            return true;
                        }
                        else {
                            return false;
                        }
                    });
                    if(index == -1) {
                        serviceLabels.push(data.Services[i].Title);
                        serviceCount.push(data.Services[i].Count);
                    }
                    else {
                        serviceCount[i] = data.Services[i].Count;
                    }
                }

                var canvas = $("#serviceChart");
        var chartOptions = {
            legend: {
                display: true,
                position: 'top',
                labels: {
                    boxWidth: 40,
                    fontColor: 'black'
                }
            }
        };

        var incomeData = {
            labels: serviceLabels,
            datasets: [{
                label: "Ordering count",
                data: serviceCount,
                borderColor: "#3cba9f",
                fill: true,
                backgroundColor: '#efe09e',
                pointRadius: 5
            }]
        };

        var lineChart = new Chart(canvas, {
            type: 'bar',
            data: incomeData,
            options: chartOptions
        });
//-----------------------------------------------------


                $("#statistic-result .card > .loading-img").addClass("hidden");
                $("#statistic-result .card > .card-body").removeClass("hidden");
            }
        });
    });
});
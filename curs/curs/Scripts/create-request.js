﻿$(document).ready(function() {
    if(location.pathname == "/Requests/Create") {
$("input[type='datetime']").flatpickr({
        enableTime: true,
        dateFormat: "Y.m.d H:i:S",
        altInput: true,
        minDate: new Date()
    });
    }
    $("#roomTypeId").on("change", function() {
        var selectedId = $("#roomTypeId option:selected").attr("value");
        var arrivalDate = $("input[name='ArrivalDate']").val();
        var departureDate = $("input[name='DepartureDate']").val();
if(location.pathname.split("/")[2] == "Edit") {
var date = arrivalDate.split(" ");
var datePart = date[0].split(".");
arrivalDate = datePart[2] + "." + datePart[1] + "." + datePart[0] + " " + date[1];

date = departureDate.split(" ");
datePart = date[0].split(".");
departureDate = datePart[2] + "." + datePart[1] + "." + datePart[0] + " " + date[1];
 }
        $.ajax({
            url: "/Rooms/GetFree",
            method: "GET",
            data: {"RoomTypeId": selectedId, "ArrivalDate": arrivalDate, "DepartureDate": departureDate},
            success: function(data) {
                var str = "";
                $.each(data, function(index, value) {
                    str += "<option value='" + value.id + "'>" + value.number + "</option>";
                });
                $("#roomId").html(str);
            }
        });
    });
});
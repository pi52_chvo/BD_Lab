﻿using curs.Models.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace curs.Repositoryes
{
    public class ServiceRepository : IBasicRepository<Service>
    {
        private HotelContext db;

        public ServiceRepository(HotelContext db)
        {
            this.db = db;
        }

        public void Delete(Service entity)
        {
            db.Services.Remove(entity);
        }

        public IEnumerable<Service> Get()
        {
            return db.Services.ToList();
        }

        public IEnumerable<Service> Get(Func<Service, bool> exprecion)
        {
            return db.Services.Where(exprecion).ToList();
        }

        public Service GetById(int id)
        {
            return db.Services.Find(id);
        }

        public void Save(Service entity)
        {
            db.Services.Add(entity);
        }

        public void Update(Service entity)
        {
            db.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }

        public SelectList GetDropdownList()
        {
            var servicesSelectList = new List<SelectListItem>();
            foreach (Service service in db.Services.ToList())
            {
                var item = new SelectListItem
                {
                    Selected = false,
                    Text = service.Title,
                    Value = service.Id.ToString()
                };
                servicesSelectList.Add(item);
            }
            return new SelectList(servicesSelectList, "Value", "Text");
        }
    }
}
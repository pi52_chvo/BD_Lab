﻿using curs.Models.Client;
using curs.Models.Request;
using curs.Models.Room;
using curs.Models.RoomType;
using curs.Models.Service;
using curs.Models.Worker;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace curs.Repositoryes
{
    public interface IUnitOfWork
    {
        IBasicRepository<Client> Clients { get; }
        IBasicRepository<Worker> Workers { get; }
        IBasicRepository<Request> Requests { get; }
        IBasicRepository<Service> Services { get; }
        IBasicRepository<Room> Rooms { get; }
        IBasicRepository<RoomType> RoomTypes { get; }
        Database Database { get; } 
        void Dispose();
        void Save();
    }
}
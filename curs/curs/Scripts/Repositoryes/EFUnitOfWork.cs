﻿using curs.Models.Client;
using curs.Models.Request;
using curs.Models.Room;
using curs.Models.RoomType;
using curs.Models.Service;
using curs.Models.Worker;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace curs.Repositoryes
{
    public class EFUnitOfWork : IUnitOfWork
    {
        private HotelContext db;
        private ClientRepository clientRepository;
        private WorkerRepository workerRepository;
        private RequestRepository requestRepository;
        private RoomRepository roomRepository;
        private RoomTypeRepository roomTypeRepository;
        private ServiceRepository serviceRepository;

        public EFUnitOfWork()
        {
            db = new HotelContext();
        }

        public IBasicRepository<Client> Clients
        {
            get
            {
                if (clientRepository == null)
                {
                    clientRepository = new ClientRepository(db);
                }
                return clientRepository;
            }
        }

        public IBasicRepository<Worker> Workers
        {
            get
            {
                if (workerRepository == null)
                {
                    workerRepository = new WorkerRepository(db);
                }
                return workerRepository;
            }
        }

        public IBasicRepository<Request> Requests
        {
            get
            {
                if (requestRepository == null)
                {
                    requestRepository = new RequestRepository(db);
                }
                return requestRepository;
            }
        }
        
        public IBasicRepository<Service> Services
        {
            get
            {
                if (serviceRepository == null)
                {
                    serviceRepository = new ServiceRepository(db);
                }
                return serviceRepository;
            }
        }
        
        public IBasicRepository<Room> Rooms
        {
            get
            {
                if (roomRepository == null)
                {
                    roomRepository = new RoomRepository(db);
                }
                return roomRepository;
            }
        }
        
        public IBasicRepository<RoomType> RoomTypes
        {
            get
            {
                if (roomTypeRepository == null)
                {
                    roomTypeRepository = new RoomTypeRepository(db);
                }
                return roomTypeRepository;
            }
        }

        public Database Database
        {
            get
            {
                return db.Database;
            }
        }

        public void Dispose()
        {
            db.Dispose();
        }

        public void Save()
        {
            db.SaveChanges();
        }
    }
}
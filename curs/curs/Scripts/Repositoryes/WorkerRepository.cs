﻿using curs.Models.Worker;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace curs.Repositoryes
{
    public class WorkerRepository : IBasicRepository<Worker>
    {
        private HotelContext db;

        public WorkerRepository(HotelContext db)
        {
            this.db = db;
        }

        public void Delete(Worker entity)
        {
            db.Users.Remove(entity);
        }

        public IEnumerable<Worker> Get()
        {
            return db.Users.ToList();
        }

        public IEnumerable<Worker> Get(Func<Worker, bool> exprecion)
        {
            return db.Users.Where(exprecion).ToList();
        }

        public Worker GetById(int id)
        {
            return db.Users.Find(id);
        }

        public void Save(Worker entity)
        {
            db.Users.Add(entity);
        }

        public void Update(Worker entity)
        {
            db.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }
    }
}
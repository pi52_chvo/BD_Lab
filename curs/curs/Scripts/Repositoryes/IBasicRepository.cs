﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace curs.Repositoryes
{
    public interface IBasicRepository<T> where T : class
    {
        void Save(T entity);
        IEnumerable<T> Get();
        IEnumerable<T> Get(Func<T, bool> exprecion);
        T GetById(int id);
        void Delete(T entity);
        void Update(T entity);
    }
}
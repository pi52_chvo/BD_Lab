﻿using curs.Models.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace curs.Repositoryes
{
    public class RequestRepository : IBasicRepository<Request>
    {
        private HotelContext db;

        public RequestRepository(HotelContext db)
        {
            this.db = db;
        }

        public void Delete(Request entity)
        {
            db.Requests.Remove(entity);
        }

        public IEnumerable<Request> Get()
        {
            return db.Requests.ToList();
        }

        public IEnumerable<Request> Get(Func<Request, bool> exprecion)
        {
            return db.Requests.Where(exprecion).ToList();
        }

        public Request GetById(int id)
        {
            return db.Requests.Find(id);
        }

        public void Save(Request entity)
        {
            db.Requests.Add(entity);
        }

        public void Update(Request entity)
        {
            db.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }
    }
}
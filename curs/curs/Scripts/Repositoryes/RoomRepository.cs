﻿using curs.Models;
using curs.Models.Room;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace curs.Repositoryes
{
    public class RoomRepository : IBasicRepository<Room>
    {
        private HotelContext db;

        public RoomRepository(HotelContext db)
        {
            this.db = db;
        }

        public void Delete(Room entity)
        {
            db.Rooms.Remove(entity);
        }

        public IEnumerable<Room> Get()
        {
            return db.Rooms.ToList();
        }

        public IEnumerable<Room> Get(Func<Room, bool> exprecion)
        {
            return db.Rooms.Where(exprecion).ToList();
        }

        public Room GetById(int id)
        {
            return db.Rooms.Find(id);
        }

        public void Save(Room entity)
        {
            db.Rooms.Add(entity);
        }

        public void Update(Room entity)
        {
            db.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }

        public SelectList GetDropdownList()
        {
            var roomsSelectList = new List<SelectListItem>();
            foreach (Room roomType in db.Rooms.ToList())
            {
                var item = new SelectListItem
                {
                    Selected = false,
                    Text = roomType.Number,
                    Value = roomType.Id.ToString()
                };
                roomsSelectList.Add(item);
            }
            return new SelectList(roomsSelectList, "Value", "Text");
        }
    }
}
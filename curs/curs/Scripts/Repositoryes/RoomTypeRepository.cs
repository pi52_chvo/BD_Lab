﻿using curs.Models.RoomType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace curs.Repositoryes
{
    public class RoomTypeRepository : IBasicRepository<RoomType>
    {
        private HotelContext db;

        public RoomTypeRepository(HotelContext db)
        {
            this.db = db;
        }

        public void Delete(RoomType entity)
        {
            db.RoomTypes.Remove(entity);
        }

        public IEnumerable<RoomType> Get()
        {
            return db.RoomTypes.ToList();
        }

        public IEnumerable<RoomType> Get(Func<RoomType, bool> exprecion)
        {
            return db.RoomTypes.Where(exprecion).ToList();
        }

        public RoomType GetById(int id)
        {
            return db.RoomTypes.Find(id);
        }

        public void Save(RoomType entity)
        {
            db.RoomTypes.Add(entity);
        }

        public void Update(RoomType entity)
        {
            db.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }

        public SelectList GetDropdownList(Func<RoomType, bool> exprecion)
        {
            var roomTypesSelectList = new List<SelectListItem>();
            foreach (RoomType roomType in db.RoomTypes.Where(exprecion))
            {
                var item = new SelectListItem
                {
                    Selected = false,
                    Text = roomType.Title,
                    Value = roomType.Id.ToString()
                };
                roomTypesSelectList.Add(item);
            }
            return new SelectList(roomTypesSelectList, "Value", "Text");
        }
        public SelectList GetDropdownList()
        {
            var roomTypesSelectList = new List<SelectListItem>();
            foreach (RoomType roomType in db.RoomTypes.ToList())
            {
                var item = new SelectListItem
                {
                    Selected = false,
                    Text = roomType.Title,
                    Value = roomType.Id.ToString()
                };
                roomTypesSelectList.Add(item);
            }
            return new SelectList(roomTypesSelectList, "Value", "Text");
        }
        public SelectList GetDropdownList(int id)
        {
            var roomTypesSelectList = new List<SelectListItem>();
            foreach (RoomType roomType in db.RoomTypes.ToList())
            {
                var item = new SelectListItem
                {
                    Selected = false,
                    Text = roomType.Title,
                    Value = roomType.Id.ToString()
                };
                if(roomType.Id == id)
                {
                    item.Selected = true;
                }
                roomTypesSelectList.Add(item);
            }
            return new SelectList(roomTypesSelectList, "Value", "Text");
        }
    }
}
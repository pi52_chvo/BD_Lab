﻿using curs.Models.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace curs.Repositoryes
{
    public class ClientRepository : IBasicRepository<Client>
    {
        private HotelContext db;

        public ClientRepository(HotelContext db)
        {
            this.db = db;
        }

        public void Delete(Client entity)
        {
            db.Clients.Remove(entity);
        }

        public IEnumerable<Client> Get()
        {
            return db.Clients.ToList();
        }

        public IEnumerable<Client> Get(Func<Client, bool> exprecion)
        {
            return db.Clients.Where(exprecion).ToList();
        }

        public Client GetById(int id)
        {
            return db.Clients.Find(id);
        }

        public void Save(Client entity)
        {
            db.Clients.Add(entity);
        }

        public void Update(Client entity)
        {
            db.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }
    }
}
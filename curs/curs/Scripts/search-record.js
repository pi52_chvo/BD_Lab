﻿$(document).ready(function() {



    $("#search-button").on("click", function(event) {
        event.preventDefault();
        if($(event.target).text() == " Find") {
            $("#search-col").fadeIn(300, function() {});
            $(event.target).html("<i class='fa fa-spinner'></i> Close");
        }
        else {
            $("#search-col").fadeOut(300, function() {});
            $(event.target).html("<i class='fa fa-spinner'></i> Find");
        }
    });
    $("#search-form").on("submit", function(event)
    {
        event.preventDefault();
        $("table").parent().find(".loading-img").removeClass("hidden");
        $("table tbody").addClass("hidden");
        var inputList = $("#search-form input");
        var selectList = $("#search-form select[data-search-type='Select']");
        var option = {};
        $.each(selectList, function(index, field) {
            var selected = $(field).find("option:selected");
            var result = "";
            $.each(selected, function(index, val) {
                result += $(val).val() + " ";
            });
            option[$(field).attr("name")] = result.trim();
        }); 
        $.each(inputList, function(index, field) {
            if($(field).attr("type") === "hidden") {
                var val = $(field).parent().find("select option:selected").val();
                switch(val) {
                    case "1":
                        if($(field).val().length) {
                            var date = $(field).val().split('.');
                            console.log(date);
                            option[$(field).attr("name")] = date[0];
                        }
                        break;
                    case "2":
                        if($(field).val().length) {
                            var date = $(field).val().split('.');
                            option[$(field).attr("name")] = date[0]+"."+date[1];
                        }
                        break;
                    case "3":
                        if($(field).val().length) {
                            var date = $(field).val().split('.');
                            option[$(field).attr("name")] = date[0]+"."+date[1]+"."+date[2].split(" ")[0];
                        }
                        break;
                    case "4":
                        if($(field).val().length) {
                           option[$(field).attr("name")] = $(field).val();
                        }
                        break;
                }
            }
            else if($(field).attr("type") === "radio") {
                var name = $(field).attr("name");
                var val = $("input[name='"+name+"']:checked").val();
                if(val == 0) {
                    option[$(field).attr("name")] = true;
                }
                else if(val == 1) {
                    option[$(field).attr("name")] = false;
                }
            }
            else if($(field).attr("data-search-type")) {
                var val = $(field).parent().find("select option:selected").val();
                switch(val) {
                    case "1":
                        if($(field).val().length) {
                            var date = $(field).val();
                            option[$(field).attr("name")] = "= " + date;
                        }
                        break;
                    case "2":
                        if($(field).val().length) {
                            var date = $(field).val();
                            option[$(field).attr("name")] = "> " + date;
                        }
                        break;
                    case "3":
                        if($(field).val().length) {
                            var date = $(field).val();
                            option[$(field).attr("name")] = "< " + date;
                        }
                        break;
                }
            }
            else {
                option[$(field).attr("name")] = $(field).val();
            }
        });
        console.log(option);
        var controllerName = location.href.split('/')[3];
        $.ajax({
            url: "/"+controllerName+"/SearchByOption",
            method: "GET",
            data: option,
            success: function(data) {
                var tbody = "";
                for(var i = 0; i < data.length; i++) {
                    var record = $("#record").html();
                    for(var key in data[i]) {
                        var regexp = new RegExp("\\["+key+"\\]", 'g');
                        console.log(data[i][key].toString());
                        record = record.replace(regexp, data[i][key]);
                    }
                    tbody += record
                }
                $("table tbody").html(tbody);
                $(".loading-img").addClass("hidden");
                $("table tbody").removeClass("hidden");
            }
        });
    });

$("input[type='datetime']").flatpickr({
        enableTime: true,
        dateFormat: "Y.m.d H:i:S",
        altInput: true
    });
});
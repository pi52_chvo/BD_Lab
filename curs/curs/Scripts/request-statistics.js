﻿$(document).ready(function() {

$("#incomeYear").on("change", function(event) {
    var year = $(event.target).val();
    $("#incomeYear").parent().find("img").removeClass("hidden");
    $.ajax({
        url: "/Requests/IncomesByYear",
        method: "GET",
        data: {"year": year},
        success: function(data) {
        $("#incomeYear").parent().find("img").addClass("hidden");
console.log(data);
        labels = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
        datas = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        $.each(data, function(index, value) {
            datas[value.Month-1] = value.Spent;
        });
        var canvas = $("#incomsChart");
        var chartOptions = {
            legend: {
                display: true,
                position: 'top',
                labels: {
                    boxWidth: 40,
                    fontColor: 'black'
                }
            }
        };

        var incomeData = {
            labels: labels,
            datasets: [{
                label: "Total spent ($)",
                data: datas,
                borderColor: "#3cba9f",
                fill: true,
                backgroundColor: 'rgba(60, 186, 159, 0.50)',
                pointRadius: 5
            }]
        };

        var lineChart = new Chart(canvas, {
            type: 'line',
            data: incomeData,
            options: chartOptions
        });
        }
    });

});


$("#clientYear").on("change", function(event) {
    var year = $(event.target).val();
    $("#clientYear").parent().find("img").removeClass("hidden");
    $.ajax({
        url: "/Requests/ClientsByYear",
        method: "GET",
        data: {"year": year},
        success: function(data) {
        $("#clientYear").parent().find("img").addClass("hidden");
        console.log(data);
        labels = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
        datas = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        $.each(data, function(index, value) {
            datas[value.Month-1] = value.Count;
        });
        var canvas = $("#clientsChart");
        var chartOptions = {
            legend: {
                display: true,
                position: 'top',
                labels: {
                    boxWidth: 40,
                    fontColor: 'black'
                }
            }
        };

        var incomeData = {
            labels: labels,
            datasets: [{
                label: "Clients count",
                data: datas,
                borderColor: "#c45850",
                backgroundColor: 'rgba(255, 0, 0, 0.5)',
                fill: true,
                pointStyle: "rectRot",
                pointRadius: 10
            }]
        };

        var lineChart = new Chart(canvas, {
            type: 'bar',
            data: incomeData,
            options: chartOptions
        });
        }
    });
});


$.ajax({
    url: "/Requests/IncomesByYear",
    method: "GET",
    data: {"year": new Date().getFullYear()},
    success: function(data) {
        console.log(data);
        labels = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
        datas = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        $.each(data, function(index, value) {
            datas[value.Month-1] = value.Spent;
        });
        var canvas = $("#incomsChart");
        var chartOptions = {
            legend: {
                display: true,
                position: 'top',
                labels: {
                    boxWidth: 40,
                    fontColor: 'black'
                }
            }
        };

        var incomeData = {
            labels: labels,
            datasets: [{
                label: "Total spent ($)",
                data: datas,
                borderColor: "#3cba9f",
                fill: true,
                backgroundColor: 'rgba(60, 186, 159, 0.50)',
                pointRadius: 5
            }]
        };

        var lineChart = new Chart(canvas, {
            type: 'line',
            data: incomeData,
            options: chartOptions
        });

        canvas.removeClass("hidden");
        $(canvas.parent().find("p")[0]).addClass("hidden");
    }
});

$.ajax({
    url: "/Requests/ClientsByYear",
    method: "GET",
    data: {"year": new Date().getFullYear()},
    success: function(data) {
        console.log(data);
        labels = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
        datas = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        $.each(data, function(index, value) {
            datas[value.Month-1] = value.Count;
        });
        var canvas = $("#clientsChart");
        var chartOptions = {
            legend: {
                display: true,
                position: 'top',
                labels: {
                    boxWidth: 40,
                    fontColor: 'black'
                }
            }
        };

        var incomeData = {
            labels: labels,
            datasets: [{
                label: "Clients count",
                data: datas,
                borderColor: "#c45850",
                backgroundColor: 'rgba(255, 0, 0, 0.5)',
                fill: true,
                pointStyle: "rectRot",
                pointRadius: 10
            }]
        };

        var lineChart = new Chart(canvas, {
            type: 'bar',
            data: incomeData,
            options: chartOptions
        });

        canvas.removeClass("hidden");
        $(canvas.parent().find("p")[0]).addClass("hidden");
    }
});

});
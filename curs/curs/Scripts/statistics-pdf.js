﻿$(document).ready(function() {
    var type;
    var year;
    var month;
    $("#statistics-button").on("click", function(event) {
        event.preventDefault();
        $('#myModal').modal('show')
    });
    $("#myModal input[name='statisticsType']").on("change", function(){
        type = $("#myModal input[name='statisticsType']:checked").val();
        if(type == "Year") {
            $("#myModal input[name='month']").attr("disabled", true);
        }
        else {
            $("#myModal input[name='month']").attr("disabled", false);
        }
    });
    $("#sendStatistics").on("click", function() {
        type = $("#myModal input[name='statisticsType']:checked").val();
        year = $("#myModal input[name='year']").val();
        month = $("#myModal input[name='month']").val();
        location = location.origin + "/Requests/GetStatistics?type=" + type + "&year=" + year + "&month=" + month;
    });
});
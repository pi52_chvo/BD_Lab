﻿$(document).ready(function() {
    
    $("input[type='datetime']").flatpickr({
        enableTime: true,
        dateFormat: "d.m.Y H:i:S",
        altInput: true
    });
});
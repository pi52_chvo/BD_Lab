﻿$(document).ready(function() {
    $("table").on("click", "th", function() {
        $("table").parent().find(".loading-img").removeClass("hidden");
        $("table tbody").addClass("hidden");
        var field = $(this).text().trim();
        var orderMethod = $(this).attr("data-order");
        if(orderMethod.length == 0) {
            orderMethod = "asc";
        }
        else if (orderMethod == "asc") {
            orderMethod = "desc";
        }
        else {
            orderMethod = "asc";
        }
        $.each($("table th"), function(index, th) {
            $(th).find("i.fa").remove();
            $(this).attr("data-order", "");
        });
        $(this).find("i.fa").remove();
        $(this).append("<i class='fa fa-sort-"+orderMethod+" float-right'></i>");
        $(this).attr("data-order", orderMethod)
        console.log(orderMethod);
        var controllerName = location.href.split('/')[3];
         $.ajax({
            url: "/"+controllerName+"/GetOrdered",
            method: "GET",
            data: {"field": field, "orderType": orderMethod},
            success: function(data) {
                var tbody = "";
                for(var i = 0; i < data.length; i++) {
                    var record = $("#record").html();
                    for(var key in data[i]) {
                        var regexp = new RegExp("\\["+key+"\\]", 'g');
                        console.log(data[i][key].toString());
                        record = record.replace(regexp, data[i][key]);
                    }
                    tbody += record
                }
                $("table tbody").html(tbody);
                $(".loading-img").addClass("hidden");
                $("table tbody").removeClass("hidden");
            }
        });
    });
});
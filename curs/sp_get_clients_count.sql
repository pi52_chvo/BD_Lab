CREATE PROCEDURE sp_get_clients_count(@year int)
AS
BEGIN
	SELECT DATEPART(MONTH, r.ReservationDate) as Month, COUNT(c.Id) as Count FROM Requests AS r 
	INNER JOIN Clients AS c ON c.Id = r.Client_Id
		WHERE DATEPART(YEAR, r.ArrivalDate) = @year
		GROUP BY DATEPART(MONTH, r.ReservationDate)
END

exec sp_get_clients_count 2017
CREATE PROCEDURE sp_get_income(@year int)
AS
BEGIN
	SELECT m.Month, SUM(m.Spent) as Spent FROM (
select DATEPART(MONTH, r.ReservationDate) AS Month, coalesce((SUM(os.Price) * DATEDIFF(DAY, r.ArrivalDate, r.DepartureDate)) + rp.Price, (SUM(os.Price) * DATEDIFF(DAY, r.ArrivalDate, r.DepartureDate)), rp.Price) as Spent from Requests as r
	left join OrderedServiceRequests as osr on r.Id = osr.Request_Id
	left join OrderedServices as os on osr.OrderedService_Id = os.Id
	left join (select r.Id, (SUM(ort.Price) * DATEDIFF(DAY, r.ArrivalDate, r.DepartureDate)) as Price from Requests as r
	left join OrderedRoomTypeRequests as ortr on r.Id = ortr.Request_Id
	left join OrderedRoomTypes as ort on ortr.OrderedRoomType_Id = ort.Id
	group by r.Id, r.ArrivalDate, r.DepartureDate) as rp on rp.Id = r.Id 
	WHERE DATEPART(YEAR, r.ReservationDate) = @year
	group by DATEPART(MONTH, r.ReservationDate),  r.ArrivalDate, r.DepartureDate, rp.Price) as m
	where Spent is not null
		GROUP BY m.Month
END

exec sp_get_income 2017



SELECT DATEPART(MONTH, rq.ReservationDate) AS Month, (ort.Price+SUM(os.Price)) * DATEDIFF(DAY, rq.ArrivalDate, rq.DepartureDate) AS Spent
			FROM Requests AS rq 
				INNER JOIN RoomRequests AS rrq ON rq.Id = rrq.Request_Id
				INNER JOIN Rooms AS r ON r.Id = rrq.Room_Id 
				INNER JOIN OrderedRoomTypeRequests AS ortr ON rq.Id = ortr.Request_Id
				INNER JOIN OrderedRoomTypes AS ort ON ort.Id = ortr.OrderedRoomType_Id
				LEFT JOIN OrderedServiceRequests AS osr ON osr.Request_Id = rq.Id
				LEFT JOIN OrderedServices AS os on os.Id = osr.OrderedService_Id 
					WHERE DATEPART(YEAR, rq.ReservationDate) = 2017
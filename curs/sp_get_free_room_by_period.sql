CREATE PROCEDURE sp_get_free_room_by_period(@roomTypeId INT, @arrivalDate DATETIME, @departureDate DATETIME)
AS
BEGIN 
SELECT DISTINCT r.Id AS id, r.Number AS number FROM Rooms AS r
		LEFT JOIN RoomRequests AS rq ON r.Id = rq.Room_Id
		LEFT JOIN Requests AS rs ON rq.Request_Id = rs.Id
		WHERE r.RoomType_Id = @roomTypeId AND 
			((rs.ArrivalDate IS NULL AND rs.DepartureDate IS NULL)
				OR (
					((@arrivalDate NOT BETWEEN rs.ArrivalDate AND rs.DepartureDate) AND (@departureDate NOT BETWEEN rs.ArrivalDate AND rs.DepartureDate)) AND 
					((rs.ArrivalDate NOT BETWEEN @arrivalDate AND @departureDate) AND (rs.DepartureDate NOT BETWEEN @arrivalDate AND @departureDate))
				)
			)
			AND r.Id NOT IN (SELECT DISTINCT r.Id AS id FROM Rooms AS r
		LEFT JOIN RoomRequests AS rq ON r.Id = rq.Room_Id
		LEFT JOIN Requests AS rs ON rq.Request_Id = rs.Id
		WHERE r.RoomType_Id = @roomTypeId AND 
			((rs.ArrivalDate=@arrivalDate OR rs.DepartureDate=@departureDate)
				OR (
				((@arrivalDate BETWEEN rs.ArrivalDate AND rs.DepartureDate) OR (@departureDate BETWEEN rs.ArrivalDate AND rs.DepartureDate)) OR 
					((rs.ArrivalDate BETWEEN @arrivalDate AND @departureDate) OR (rs.DepartureDate BETWEEN @arrivalDate AND @departureDate))
				)
			))
END

select re.ArrivalDate, re.DepartureDate, r.Number from Requests as re Inner JOIN RoomRequests on re.Id = RoomRequests.Request_Id INNER JOIN Rooms as r on r.Id = RoomRequests.Room_Id

execute sp_get_free_room_by_period 2, '20171215 00:00:00', '20171229 00:00:00'



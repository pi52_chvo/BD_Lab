﻿using Cursova.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Cursova.pages
{
    /// <summary>
    /// Логика взаимодействия для CreateRoomWindow.xaml
    /// </summary>
    public partial class CreateRoomWindow : Window
    {
        private HotelContext db;

        public CreateRoomWindow()
        {
            InitializeComponent();
            db = new HotelContext();
            servicesList.ItemsSource = db.RoomTypes.ToList();
        }

        private void exit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void createRow_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int roomNumber = int.Parse(roomNumberText.Text);
                if(db.Rooms.Select(r => r.Number).Contains(roomNumber))
                {
                    throw new Exception("Комната с заданым номером уже существует!");
                }
                RoomType roomType = servicesList.SelectedItem as RoomType;
                Room newRoom = new Room
                {
                    Number = roomNumber,
                    Type = roomType,
                    IsFree = true,
                    Requests = new List<Request>()
                };
                db.Rooms.Add(newRoom);
                db.SaveChanges();
                this.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}

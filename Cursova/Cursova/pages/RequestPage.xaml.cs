﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cursova.pages
{
    /// <summary>
    /// Логика взаимодействия для RequestPage.xaml
    /// </summary>
    public partial class RequestPage : Page
    {
        private HotelContext db;

        public RequestPage()
        {
            InitializeComponent();
            var t = new Thread(new ThreadStart(() =>
            {
                db = new HotelContext();
                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)(() => { requestGrid.ItemsSource = db.Requests.ToList(); }));
            }))
            {
                IsBackground = true
            };
            t.Start();
        }
    }
}

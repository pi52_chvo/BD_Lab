﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cursova.pages
{
    /// <summary>
    /// Логика взаимодействия для WorkerPage.xaml
    /// </summary>
    public partial class WorkerPage : Page
    {
        private HotelContext db;

        public WorkerPage()
        {
            InitializeComponent();
            
            
            var t = new Thread(new ThreadStart(() =>
            {
                db = new HotelContext();
                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)(() => { workerDataGrid.ItemsSource = db.Workers.ToList(); }));
            }))
            {
                IsBackground = true
            };
            t.Start();
        }
    }
}

﻿using Cursova.models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cursova.pages
{
    /// <summary>
    /// Логика взаимодействия для RoomPage.xaml
    /// </summary>
    public partial class RoomPage : Page
    {
        private HotelContext db;

        public RoomPage()
        {
            InitializeComponent();
            
            
            var t = new Thread(new ThreadStart(() =>
            {
                db = new HotelContext();
                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)(() => 
                    {
                        roomGrid.ItemsSource = db.Rooms.ToList();
                        servicesList.ItemsSource = db.RoomTypes.ToList();
                    }));
            }))
            {
                IsBackground = true
            };
            t.Start();
        }

        private void roomNumberText_TextInput(object sender, TextCompositionEventArgs e)
        {
        }

        private void roomNumberText_TextChanged(object sender, TextChangedEventArgs e)
        {
            Regex reg = new Regex("[^0-9]");
            string correct = reg.Replace(roomNumberText.Text, "");
            roomNumberText.Text = correct;
            roomNumberText.CaretIndex = roomNumberText.Text.Length;
        }

        private void roomPriceText_TextChanged(object sender, TextChangedEventArgs e)
        {
            Regex reg = new Regex("[^0-9]");
            string correct = reg.Replace(roomPriceText.Text, "");
            roomPriceText.Text = correct;
            roomPriceText.CaretIndex = roomPriceText.Text.Length;
        }

        private void acceptFiltration_Click(object sender, RoutedEventArgs e)
        {
            List<Room> filteredList = new List<Room>();
            bool isFiltered = false;
            if(roomNumberText.Text.Length > 0)
            {
                int roomNumber = int.Parse(roomNumberText.Text);
                filteredList = db.Rooms.Where(r => r.Number == roomNumber).ToList();
                isFiltered = true;
            }
            if(roomPriceText.Text.Length > 0)
            {
                double roomPrice = double.Parse(roomPriceText.Text);
                if(filteredList.Count > 0 || isFiltered)
                {
                    filteredList = filteredList.Where(r => r.Type.Price == roomPrice).ToList();                
                }
                else
                {
                    filteredList = db.Rooms.Where(r => r.Type.Price == roomPrice).ToList();
                }
                isFiltered = true;
            }
            if (roomFree.IsChecked == true || roomNotFree.IsChecked == true)
            {
                bool roomStatus = true;
                if (roomNotFree.IsChecked == true)
                {
                    roomStatus = false;
                }
                if (filteredList.Count > 0 || isFiltered)
                {
                    filteredList = filteredList.Where(r => r.IsFree == roomStatus).ToList();
                }
                else
                {
                    filteredList = db.Rooms.Where(r => r.IsFree == roomStatus).ToList();
                }
                isFiltered = true;
            }
            if(servicesList.SelectedItems.Count > 0)
            {
                var list = servicesList.SelectedItems;
                List<RoomType> selectedTypes = new List<RoomType>();
                foreach(RoomType type in list)
                {
                    selectedTypes.Add(type);
                }
                var indexes = selectedTypes.Select(st => st.Id).ToList();
                if (filteredList.Count > 0 || isFiltered)
                {
                    filteredList = filteredList.Where(r => indexes.Contains(r.Type.Id)).ToList();
                }
                else
                {
                    filteredList = db.Rooms.Where(r => indexes.Contains(r.Type.Id)).ToList();
                }
                isFiltered = true;
            }
            if(isFiltered)
            {
                roomGrid.ItemsSource = filteredList;
            }        
        }

        private void rejectFiltration_Click(object sender, RoutedEventArgs e)
        {
            roomGrid.ItemsSource = db.Rooms.ToList();
        }

        private void createRow_Click(object sender, RoutedEventArgs e)
        {
            CreateRoomWindow crWindow = new CreateRoomWindow();
            crWindow.ShowDialog();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Cursova.models;
using Cursova.pages;
using System.Windows.Threading;
using System.Threading;

namespace Cursova
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public delegate void WindowDelegate();

        public MainWindow()
        {
            InitializeComponent();
            MainFrame.Content = new StartPage();
        }

        private void RequestButton_Click(object sender, RoutedEventArgs e)
        {    
            MainFrame.Content = new RequestPage();
        }

        private  void Button_Click(object sender, RoutedEventArgs e)
        {
            
                        MainFrame.Content = new WorkerPage();
     
        }

        private void ServiceButton_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Content = new ServicePage();
        }

        private void RoomButton_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Content = new RoomPage();
        }

        private void RoomTypeButton_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Content = new RoomTypePage();
        }

        private void ClientButton_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Content = new ClientPage();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cursova.models
{
    public class Room
    {
        public int Id { get; set; }
        public int Number { get; set; }
        public virtual RoomType Type { get; set; }
        public virtual List<Request> Requests { get; set; }
        public bool IsFree { get; set; }
    }
}

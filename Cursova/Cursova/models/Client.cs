﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cursova.models
{
    public class Client
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Phone { get; set; }
        public string PassportSeria { get; set; }
        public string PassportNumber { get; set; }
        public virtual List<Request> Requests { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cursova.models
{
    public class Request
    {
        public int Id { get; set; }
        public virtual Client Client { get; set; }
        public virtual Worker Worker { get; set; }
        public virtual Room Room { get; set; }
        public DateTime ArrivalDate { get; set; }
        public DateTime DepartureDate { get; set; }
        public DateTime RequestDate { get; set; }
        public virtual List<Service> Services { get; set; }
    }
}

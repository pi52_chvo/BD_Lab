﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cursova.models
{
    public class RoomType
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public double Price { get; set; }
        public virtual List<Room> Rooms { get; set; }
    }
}

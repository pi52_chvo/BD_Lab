create trigger teacherDelete
on Teacher after delete 
as
declare @teacher_id as int
select @teacher_id = TeacherId from deleted
if exists (select * from Teacher_Load where TeacherId = @teacher_id)
begin
	delete from Teacher_Load where TeacherId = @teacher_id
end


select * from Teacher

delete from Teacher where TeacherId = 6
select * from Teacher_Load